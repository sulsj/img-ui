#!/usr/common/usg/languages/python/2.7.3_1/bin/python

# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""

This is to submit a task to the search server with a list of oids.

Usage:

    client.py --program <program> --contact <contact_oid> --funcset <func set file name> --functype <function category> --geneset <gene set file names> --scafset <scaffold set file names> --output <output>]
"""
import uuid
import getopt
import gzip
import cPickle
import zlib
import sys
import subprocess
import re
import os
import time

try:
    import pika
except Exception, detail:
    print "Error importing pika. "
    print "Please check your installation."
    print detail
    sys.exit(1)
    
    
def zdumps(obj):
    """
    dumps pickleable object into zlib compressed string
    """
    return zlib.compress(cPickle.dumps(obj,cPickle.HIGHEST_PROTOCOL),9)

    
def zloads(zstr):
    """
    loads pickleable object from zlib compressed string
    """
    return cPickle.loads(zlib.decompress(zstr))


def is_running(process):
    """
    Check "process" is runinng
    
    @param process: process name
    @type string
    """
    s = subprocess.Popen(["ps", "axw"],stdout=subprocess.PIPE)
    for x in s.stdout:
        if re.search(process, x):
            return True

    return False


def usage():
    print "Usage: client.py --program <program> --contact <contact_oid> "
    print "           [--funcset <function set name> | --functype <function category>] "
    print "           [--geneset <gene set names>] [--scafset <scaffold set names>] "
    print "           --output <output dir name>"
    print "           ex) client.py --program geneFuncSetProfile --contact 312 --funcset 3cog --geneset etoliko2 --output test2"


def main(argv):
    """
    Parse the command line inputs and call send_info 
        
    @param argv: list of arguments
    @type argv: list of strings
    """
    
    oids = []
    outFileName = ""
    contactOid = ""
    funcSetName = ""
    funcTypeName = ""
    progName = ""
    scafName = ""
    geneName = ""

    try:
        opts, args = getopt.getopt(argv[1:],"hi:o:k:s:c:f:t:",["help","oidfile=","output=","geneset=","scafset=","program=","contact=","functype=","funcset="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-k", "--geneset"):
            #oids = arg.split(',')
            geneName = arg
            oids.append(arg)
        elif opt in ("-s", "--scafset"):
            #oids = arg.split(',')
            scafName = arg
            oids.append(arg)
        elif opt in ("-i", "--oidfile"):
            try:
                #print "Loading oids form file,", arg 
                fh = open(arg, "r")
                for line in fh: 
                    if len(line.strip()) > 0:
                        oids.append(line.strip())
                fh.close()
                #print "Total number of oids loaded: ", len(oids)
            except IOError:
                print >> sys.stderr, "ERROR: Cannot open oid file"
        elif opt in ("-c", "--contact"):
            contactOid = arg
        elif opt in ("-f", "--funcset"):
            funcSetName = arg
        elif opt in ("-t", "--functype"):
            funcTypeName = arg
        elif opt in ("-o", "--output"):
            outFileName = arg
        elif opt in ("-p", "--program"):
            progName = arg
        else:
            usage()
            sys.exit(2)

    if not len(contactOid) > 0:
        print "Please specify contact_oid to search"
        usage()
        sys.exit(1)
    elif not len(progName) > 0:
        print "Please specify program name"
        usage()
        sys.exit(1)
    elif not len(outFileName) > 0:
        print "Please specify output name"
        usage()
        sys.exit(1)
    elif not len(funcSetName) > 0 and not len(funcTypeName) > 0 and progName in ('geneFuncSetProfile', 'scafFuncSetProfile'):
        print "Please specify function set name or category to search"
        usage()
        sys.exit(1)
    elif not len(oids) > 0:
        print "Please specify gene set name to search"
        usage()
        sys.exit(1)
    else:
        print "Total number of jobs: ", len(oids)
            
    ## localhost
    #conn = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    ## remote broker (carver)
    #credentials = pika.PlainCredentials('img', 'img')
    #parameters = pika.ConnectionParameters(credentials=credentials,
    #                                       host='128.55.57.16',
    #                                       virtual_host='img_ui')
    ## remote broker (mq.nersc.gov)
	credentials = pika.PlainCredentials('img', 'synchrotron')
	parameters = pika.ConnectionParameters(credentials=credentials,
                                           host='mq.nersc.gov',
                                           virtual_host='jgi')

    conn = pika.BlockingConnection(parameters)
    ch = conn.channel()
    
    ## Declare task sending queue (client --> worker)
    ch.queue_declare(queue='img_task_queue', durable=True)
    
    ##
    ## UPDATE: Now the worker does not return the result back to the requesting client.
    ## No need to prepare an unique return queue
    ##
    ## Declare result recv queue (worker --> client)
    #queue_name = str(uuid.uuid4())
    #result = ch.queue_declare(queue=queue_name, durable=True)
    ## UPDATE: set the queue as auto_delete mode
    #result = ch.queue_declare(queue=queue_name, durable=False, exclusive=False, auto_delete=True)
    #callback_queue = result.method.queue
    #corr_id = str(uuid.uuid4())
    #print "Queue:  ", queue_name
    #print "Tag_id: ", corr_id
    
    for oid in oids:
        msgContainer = {}
        msgContainer["contact"] = contactOid
        msgContainer["funcset"] = funcSetName
        msgContainer["functype"] = funcTypeName
        msgContainer["output"] = outFileName
        msgContainer["program"] = progName

        if progName in ( 'geneFuncSetProfile' ):
            msgContainer["geneset"] = oid
        else:
            msgContainer["scafset"] = oid

        msgContainer["data"] = "" ## reserved for specifying the Perl module name to use
        msg_zipped = zdumps(msgContainer)
                
        ch.basic_qos(prefetch_count=1) ## how many messages send to server
        try:
            ch.basic_publish(exchange='',
                              routing_key='img_task_queue',
                              body=msg_zipped,
                              properties=pika.BasicProperties(
                                    delivery_mode = 2, ## make message persistent
                                    #reply_to = callback_queue, ## set returning queue name
                                    #correlation_id = corr_id,
                              ))
        except:
            print "Failed to submit a request"
            sys.exit(1)
        
    conn.close()


if __name__ == "__main__":

    ## Check if the worker is running.
    ## If not, start the worker process
#    worker_process_name = "worker.py"
#    if not is_running(worker_process_name):
#        print "The worker process is not running now. Trying to start the process."
#        os.system(os.getcwd() + "/worker.py &")
#        time.sleep(3)
#        if not is_running(worker_process_name):
#            print "Failed to start the worker process."
#            sys.exit(1)
#        else:
#            print "The worker is ready. Send the request to the worker."
        
    sys.exit(main(sys.argv))
    
    
# EOF
