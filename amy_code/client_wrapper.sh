#!/bin/bash -l

##################
# module load python
##################

function usage
{
    echo "Usage: client_wrapper.sh --program (program) --contact (contact_oid) --funcset (function set name) --functype (function category) --geneset (gene set names) --scafset (scaffold set names) --output (output dir name)"

}

if [ $# == 0 ]; then
    echo "no parameters"
    usage
    exit 
fi

gene_set=
scaf_set=
funcSetName=""
funcTypeName=""
contactOid=
outFileName=
progName=

while [ "$1" != "" ]; do
    case $1 in
        -c | --contact )        shift
                                contactOid=$1
                                ;;
        -f | --funcset )        shift
                                funcSetName=$1
                                ;;
        -t | --functype )       shift
                                funcTypeName=$1
                                ;;
        -k | --geneset )        shift 
                                gene_set=$1
                                ;;
        -s | --scafset )        shift 
                                scaf_set=$1
                                ;;
        -o | --output )         shift
                                outFileName=$1
                                ;;
        -p | --program )        shift
                                progName=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
	client_wrapper.sh )     ;;
        * )                     usage
                                exit 1
    esac

    shift

done

export LD_LIBRARY_PATH=/usr/common//usg/languages/python/2.7.3_1/lib/:/usr/common/usg/utility_libraries/readline/6.2/lib/ 

PYTHONEXE="/usr/common/usg/languages/python/2.7.3_1/bin/python"
#PYTHONEXE="/jgi/tools/bin/python"

WORKDIR="/house/groupdirs/img/public-web/vhosts/img-stage.jgi-psf.org/cgi-bin/img_amy/"

if [ "$progName" == "geneFuncSetProfile" ]; then
    if [ "$funcTypeName" != "" ]; then
	$PYTHONEXE $WORKDIR/client.py --program $progName --contact $contactOid --functype $funcTypeName --geneset $gene_set --output $outFileName
    elif [ "$funcSetName" != "" ]; then
	$PYTHONEXE $WORKDIR/client.py --program $progName --contact $contactOid --funcset $funcSetName --geneset $gene_set --output $outFileName
    fi
elif [ "$progName" == "scafFuncSetProfile" ]; then
    if [ "$funcTypeName" != "" ]; then
	$PYTHONEXE $WORKDIR/client.py --program $progName --contact $contactOid --functype $funcTypeName --scafset $scaf_set --output $outFileName
    elif [ "$funcSetName" != "" ]; then
	$PYTHONEXE $WORKDIR/client.py --program $progName --contact $contactOid --funcset $funcSetName --scafset $scaf_set --output $outFileName
    fi
elif [ "$progName" == "scafPhyloDist" ]; then
    $PYTHONEXE $WORKDIR/client.py --program $progName --contact $contactOid --scafset $scaf_set --output $outFileName
fi
ret=$?

exit $ret

# EOF
