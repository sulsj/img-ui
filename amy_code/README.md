The scripts in this directory is to maintain a certain number of workers running
on a node. If the number of jobs queued in the RabbitMQ message queue becomes
larger that the number of workers, more workers will be submitted the worker jobs
to the cluster. 
SCRIPTS

- check_running_img_worker.sh

  Main script to control the number of workers
  dynamically. Need two parameters to start: 1) the name of the worker process
  for executing "grep" to check the number of worker processes on a node, 2)
  minimum number of workers to run on a node (not on cluster)
  
- check_num_tasks_waiting.py

  Python script to get the number of jobs waiting
  which is called in the script, "check_running_img_worker.sh". Need a parameter,
  "queue name" from which user needs to get the number of waiting jobs. This
  is currently fixed as "img_task_queue".
  The difference between this number and the number of workers currently running
  on a node and cluster is the number of additional workers to add.
  
- run_img_worker.sh

  To run a worker on a cluster
  
  
OPERATION STEPS:

 1. Check how many workers running now with "worker process name"
 2. If the number of workers are smaller than <min num workers>, start more workers
 3. Check how many tasks are queued in the img_task_queue. If there are smaller number
    of workers are queued/runnning, add more workers. The number of workers to add is
    determined based on the NTPW parameter
 4. If need to start more workers, it does qsub to submit worker jobs if
    there is no such jobs queued/running. 
