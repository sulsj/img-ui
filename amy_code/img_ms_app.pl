#!/usr/bin/env perl
######################################################################
# img_ms_app.pl: IMG application for message system
#
######################################################################

use POSIX qw(ceil floor);
use strict;
use Archive::Zip;
use FileHandle;
use BerkeleyDB;
use HashUtil;
use WebConfig;
use WebUtil;

use Time::localtime;
use Data::Dumper; 
use DBI; 
use MIME::Base64 qw( decode_base64 ); 

my $env = getEnv();
 
my $data_type = "assembled";
my $mer_data_dir = $env->{mer_data_dir};
my $workspace_dir = $env->{workspace_dir};
my $out_dir = $env->{workspace_dir};

my $contact_oid = 0;
my $functype = "";
my $func_set = "";
my $gene_set = "";
my $scaf_set = "";
my $out_name = "";
my $prog_name = "";
my $taxon_oid = 0; 

my $i = 0; 
while ( $i < scalar(@ARGV) ) { 
    if ( $ARGV[$i] eq "-program" ) { 
        $i++; 
        $prog_name = $ARGV[$i]; 
    } 
    elsif ( $ARGV[$i] eq "-funcset" ) {
        $i++; 
        $func_set = $ARGV[$i];
    }
    elsif ( $ARGV[$i] eq "-functype" ) {
        $i++; 
        $functype = $ARGV[$i];
    }
    elsif ( $ARGV[$i] eq "-geneset" ) {
        $i++; 
	if ( $gene_set ) {
	    $gene_set .= "," . $ARGV[$i];
	}
	else {
	    $gene_set = $ARGV[$i];
	}
    } 
    elsif ( $ARGV[$i] eq "-scafset" ) {
        $i++; 
	if ( $scaf_set ) {
	    $scaf_set .= "," . $ARGV[$i];
	}
	else {
	    $scaf_set = $ARGV[$i];
	}
    } 
    elsif ( $ARGV[$i] eq "-contact" ) {
        $i++; 
        $contact_oid = $ARGV[$i];
    } 
    elsif ( $ARGV[$i] eq "-output" ) {
        $i++; 
        $out_name = $ARGV[$i];
    } 

    $i++;
}

if ( ! $contact_oid || ! $prog_name || ! $out_name ) {
    print "usage: img_ms_app.pl -program <program> -contact <contact_oid>\n";
    print "                     -output <output file name> \n";
    print "                     [-funcset <function set name(s)> | -functype <function category>]\n";
    print "                     [-geneset <gene set name(s)>]\n";
    print "                     [-scafset <scaffold set name(s)>]\n";
    exit(2); 
} 

# create output dir
if ( !-e "$out_dir" ) {
    mkdir "$out_dir" or die("Cannot find $out_dir\n");
} 
if ( !-e "$out_dir/$contact_oid" ) { 
    mkdir "$out_dir/$contact_oid" or die("Cannot create $out_dir/$contact_oid");
} 
if ( !-e "$out_dir/$contact_oid/job" ) { 
    mkdir "$out_dir/$contact_oid/job" or die("Cannot create $out_dir/$contact_oid/job");
} 
$out_name = checkPath($out_name);
if ( !-e "$out_dir/$contact_oid/job/$out_name" ) { 
    mkdir "$out_dir/$contact_oid/job/$out_name" or die("Cannot create $out_dir/$contact_oid/job/$out_name");
} 

# clean old names
for my $fname2 ( 'done.txt', 'error.txt', 'profile.txt', 'list.txt', 'log.txt' ) {
    my $full_file_name = "$out_dir/$contact_oid/job/$out_name/" . $fname2;
    if ( -e $full_file_name ) {
	unlink($full_file_name);
    }
}

if ( $prog_name eq 'geneFuncSetProfile' ) {
    if ( $functype ) {
	geneFuncTypeProfile($contact_oid, $out_name, $functype, $gene_set);
    }
    elsif ( $func_set ) {
	geneFuncSetProfile($contact_oid, $out_name, $func_set, $gene_set);
    }
}
elsif ( $prog_name eq 'scafFuncSetProfile' ) {
    if ( $functype ) {
	scafFuncTypeProfile($contact_oid, $out_name, $functype, $scaf_set);
    }
    elsif ( $func_set ) {
	scafFuncSetProfile($contact_oid, $out_name, $func_set, $scaf_set);
    }
}
elsif ( $prog_name =~ /scafPhylo/ ) {
    scafPhyloDist($contact_oid, $out_name, $scaf_set);
}
else {
    my $error_file_name = "$out_dir/$contact_oid/job/$out_name/error.txt";
    open (ERROR_FILE, '>', $error_file_name) or exit 20;
    print ERROR_FILE "Unknown function: $prog_name\n";
    print ERROR_FILE currDateTime() . "\n";
    close ERROR_FILE;
    exit 20;
}

1;


###########################################################################################
# geneFuncTypeProfile
###########################################################################################
sub geneFuncTypeProfile {
    my ($contact_oid, $out_name, $functype, $gene_set) = @_;

    my $log_file_name = "$out_dir/$contact_oid/job/$out_name/log.txt";
    open (LOG_FILE, '>>', $log_file_name) or return;
    print LOG_FILE "geneFuncTypeProfile\n";

    if ( ! $functype ) {
	my $error_file_name = "$out_dir/$contact_oid/job/$out_name/error.txt";
	open (ERROR_FILE, '>', $error_file_name) or return;
	print ERROR_FILE "No function category\n";
	print ERROR_FILE currDateTime() . "\n";
	close ERROR_FILE;
	close LOG_FILE;
	return;
    }

    my @filenames = ();
    if ( $gene_set ) {
	@filenames = split(/\,/, $gene_set);
    }
    if ( scalar(@filenames) == 0 ) {
	my $error_file_name = "$out_dir/$contact_oid/job/$out_name/error.txt";
	open (ERROR_FILE, '>', $error_file_name) or return;
	print ERROR_FILE "No gene set\n";
	print ERROR_FILE currDateTime() . "\n";
	close ERROR_FILE;
	close LOG_FILE;
	return;
    }

    # get all functions
    my $dbh = dbLogin();
    my $k = 0;
    my @func_ids = ();
    my $sql = getFuncTypeIdSql($functype);
    if ( $sql ) {
	my %h2;
	my $cur = $dbh->prepare( $sql );
	$cur->execute();
	for (;;) {
	    my ($id2, $name2, @rest2) = $cur->fetchrow();
	    last if !$id2;

	    $h2{$id2} = $name2;

#	    $k++;
#	    if ( $k > 10 ) {
#		last;
#	    }
	}

	for my $id2 (keys %h2) {
	    push @func_ids, ( $id2 );
	}
    }

    print LOG_FILE "Function Category: $functype\n";
    print LOG_FILE "Gene Set: $gene_set\n";

    my $line = "";
    my $line_no = 0;

    my $out_file_name = "$out_dir/$contact_oid/job/$out_name/profile.txt";
    open (OUTPUT_FILE, '>', $out_file_name) or return;

    my $out_list_name = "$out_dir/$contact_oid/job/$out_name/list.txt";
    my $fh_list = new FileHandle($out_list_name, "w");

    for my $gene_set1 ( @filenames ) {
	print LOG_FILE "Processing gene set $gene_set1 ...\n";

	# read all gene oids
	my %gene_h;
	my $gset = new FileHandle("$workspace_dir/$contact_oid/gene/$gene_set1");
	while ( my $id = $gset->getline() ) {
	    chomp $id;
	    next if ( $id eq "" );
	    $gene_h{$id} = 0;
	}

	for my $func_id ( sort @func_ids ) {
	    my $cnt = computeGeneFuncProfile($dbh, $fh_list, $func_id, $gene_set1, \%gene_h);
	    print OUTPUT_FILE "$gene_set1\t$func_id\t$cnt\n";

	    # clear gene_h
	    for my $gene_oid (keys %gene_h) {
		$gene_h{$gene_oid} = 0;
	    }
	}
    }

    close OUTPUT_FILE;
    $fh_list->close();
    $dbh->disconnect();

    my $done_file = "$out_dir/$contact_oid/job/$out_name/done.txt";
    open (DONE_FILE, '>', $done_file) or return;
    print DONE_FILE currDateTime() . "\n";
    close DONE_FILE;

    print LOG_FILE currDateTime() . "\n";

    my $i = 0; 

    close LOG_FILE;
}


###########################################################################################
# geneFuncSetProfile
###########################################################################################
sub geneFuncSetProfile {
    my ($contact_oid, $out_name, $func_set, $gene_set) = @_;

    my $log_file_name = "$out_dir/$contact_oid/job/$out_name/log.txt";
    open (LOG_FILE, '>>', $log_file_name) or return;
    print LOG_FILE "geneFuncSetProfile\n";

    if ( ! $func_set ) {
	my $error_file_name = "$out_dir/$contact_oid/job/$out_name/error.txt";
	open (ERROR_FILE, '>', $error_file_name) or return;
	print ERROR_FILE "No function set\n";
	print ERROR_FILE currDateTime() . "\n";
	close ERROR_FILE;
	close LOG_FILE;
	return;
    }

    my @filenames = ();
    if ( $gene_set ) {
	@filenames = split(/\,/, $gene_set);
    }
    if ( scalar(@filenames) == 0 ) {
	my $error_file_name = "$out_dir/$contact_oid/job/$out_name/error.txt";
	open (ERROR_FILE, '>', $error_file_name) or return;
	print ERROR_FILE "No gene set\n";
	print ERROR_FILE currDateTime() . "\n";
	close ERROR_FILE;
	close LOG_FILE;
	return;
    }

    # read function set
    my @func_ids = ();
    my $res = new FileHandle("$workspace_dir/$contact_oid/function/$func_set");
    while ( my $id = $res->getline() ) {
	chomp $id;
	next if ( $id eq "" );
	push @func_ids, ($id);
    } 
    close $res;

    print LOG_FILE "Function Set: $func_set\n";
    print LOG_FILE "Gene Set: $gene_set\n";

    my $line = "";
    my $line_no = 0;

    my $out_file_name = "$out_dir/$contact_oid/job/$out_name/profile.txt";
    open (OUTPUT_FILE, '>', $out_file_name) or return;

    my $out_list_name = "$out_dir/$contact_oid/job/$out_name/list.txt";
    my $fh_list = new FileHandle($out_list_name, "w");

    my $dbh = dbLogin();
    for my $gene_set1 ( @filenames ) {
	print LOG_FILE "Processing gene set $gene_set1 ...\n";

	# read all gene oids
	my %gene_h;
	my $gset = new FileHandle("$workspace_dir/$contact_oid/gene/$gene_set1");
	while ( my $id = $gset->getline() ) {
	    chomp $id;
	    next if ( $id eq "" );
	    $gene_h{$id} = 0;
	}

	for my $func_id ( sort @func_ids ) {
	    my $cnt = computeGeneFuncProfile($dbh, $fh_list, $func_id, $gene_set1, \%gene_h);
	    print OUTPUT_FILE "$gene_set1\t$func_id\t$cnt\n";

	    # clear gene_h
	    for my $gene_oid (keys %gene_h) {
		$gene_h{$gene_oid} = 0;
	    }
	}
    }

    close OUTPUT_FILE;
    $fh_list->close();
    $dbh->disconnect();

    my $done_file = "$out_dir/$contact_oid/job/$out_name/done.txt";
    open (DONE_FILE, '>', $done_file) or return;
    print DONE_FILE currDateTime() . "\n";
    close DONE_FILE;

    print LOG_FILE currDateTime() . "\n";

    my $i = 0; 

    close LOG_FILE;
}


#######################################################################
# computeGeneFuncProfile
#######################################################################
sub computeGeneFuncProfile {
    my ($dbh, $res, $func_id, $gene_set, $gene_href) = @_;

    my %func_hash;
    undef %func_hash;

    my %gene_name_h;
    undef %gene_name_h;

    my $gene_count = 0;
    my @gene_oids = (keys %$gene_href);
    my $min_gene_oid = 0;
    my $max_gene_oid = 0;

    for my $line ( @gene_oids ) { 
	my @v         = split( / /, $line ); 
	my $gene_oid  = 0; 
	my $data_type = "database"; 
	my $taxon_oid = 0; 
 
	if ( scalar(@v) >= 3 ) { 
	    $taxon_oid = $v[0];
	    $data_type = $v[1];
	    $gene_oid  = $v[2];
	} 
	elsif ( scalar(@v) > 0 ) { 
	    $gene_oid = $v[0]; 
 
	    if ( ! $min_gene_oid || $gene_oid < $min_gene_oid ) {
		$min_gene_oid = $gene_oid;
	    } 
	    if ( $gene_oid > $max_gene_oid ) {
		$max_gene_oid = $gene_oid;
	    } 
	} 
	my $key = ""; 
	if ( $data_type eq 'assembled' || $data_type eq 'unassembled' ) {
	    $key = "$taxon_oid|$data_type";
	}
	elsif ( $data_type eq 'database' ) {
	    $key = $data_type; 
	} 
 
	if ( $func_hash{$key} ) { 
	    my $h_ref = $func_hash{$key};
	    $h_ref->{$gene_oid} = 1;
	} 
	else { 
	    my %hash2; 
	    undef %hash2; 
	    $hash2{$gene_oid} = 1;
	    $func_hash{$key}  = \%hash2;
	} 
    }  # end while line

    for my $key ( keys %func_hash ) { 
        my $h_ref = $func_hash{$key}; 
        if ( !$h_ref ) { 
            next; 
        } 
 
        if ( $key eq 'database' ) {
            # database
            my $sql   = ""; 
            my $db_id = $func_id;
            if ( $func_id =~ /COG\_Category/ ) {
                my ($id1, $id2) = split(/\:/, $func_id);
                $db_id = $id2;
                $sql = "select distinct g.gene_oid, g.cog " .
                    "from gene_cog_groups g, cog_functions cf " .
                    "where g.cog = cf.cog_id and cf.functions = ?";
            } 
            elsif ( $func_id =~ /COG\_Pathway/ ) {
                my ($id1, $id2) = split(/\:/, $func_id);
                $db_id = $id2;
                $sql = "select distinct g.gene_oid, cpcm.cog_members " .
                    "from gene_cog_groups g, cog_pathway_cog_members cpcm " .
                    "where g.cog = cpcm.cog_members and cpcm.cog_pathway_oid = ?";
            } 
            elsif ( $func_id =~ /COG/ ) { 
                $sql = "select distinct g.gene_oid, g.cog " .
		    "from gene_cog_groups g where g.cog = ?";
            }
            elsif ( $func_id =~ /Pfam\_Category/ ) {
                my ($id1, $id2) = split(/\:/, $func_id);
                $db_id = $id2;
                $sql = "select distinct g.gene_oid, g.pfam_family " .
                    "from gene_pfam_families g, pfam_family_cogs pfc " .
                    "where g.pfam_family = pfc.ext_accession " .
                    "and pfc.functions = ?";
            } 
            elsif ( $func_id =~ /pfam/ ) {
                $sql = "select distinct g.gene_oid, g.pfam_family " .
                    "from gene_pfam_families g " .
                    "where g.pfam_family = ?"; 
            } 
            elsif ( $func_id =~ /TIGRfam\_Role/ ) { 
                my ($id1, $id2) = split(/\:/, $func_id); 
                $db_id = $id2; 
                $sql = "select distinct g.gene_oid, g.ext_accession " . 
                    "from gene_tigrfams g, tigrfam_roles tr " . 
                    "where g.ext_accession = tr.ext_accession and tr.roles = ?"; 
            } 
            elsif ( $func_id =~ /TIGR/ ) { 
                $sql = "select distinct g.gene_oid, g.ext_accession " . 
                    "from gene_tigrfams g " . 
                    "where g.ext_accession = ?"; 
            } 
            elsif ( $func_id =~ /KEGG\_Category\_KO/ ) { 
                my ($id1, $id2) = split(/\:/, $func_id); 
                $db_id = $id2; 
                $sql = "select distinct g.gene_oid, kp.category " .
                    "from gene_ko_terms g, image_roi_ko_terms rk, " .
                    "image_roi ir, kegg_pathway kp " . 
                    "where g.ko_terms = rk.ko_terms " . 
                    "and rk.roi_id = ir.roi_id " . 
                    "and ir.pathway = kp.pathway_oid " .
                    "and kp.category = (select kp3.category from kegg_pathway kp3 " .
                    "where kp3.pathway_oid = ?) ";
            } 
            elsif ( $func_id =~ /KEGG\_Pathway\_KO/ ) { 
                my ($id1, $id2) = split(/\:/, $func_id); 
                $db_id = $id2;
                $sql = "select g.gene_oid, ir.pathway " .
                    "from gene_ko_terms g, image_roi_ko_terms rk, " .
                    "image_roi ir " .
                    "where g.ko_terms = rk.ko_terms " . 
                    "and rk.roi_id = ir.roi_id " . 
                    "and ir.pathway = ?"; 
            } 
            elsif ( $func_id =~ /KOG/ ) { 
                $sql = "select distinct g.gene_oid, g.kog " .
		    "from gene_kog_groups g where g.kog = ?";
            } 
            elsif ( $func_id =~ /KO/ ) { 
                $sql = "select distinct g.gene_oid, g.ko_terms " . 
                    "from gene_ko_terms g " . 
                    "where g.ko_terms = ?"; 
            } 
            elsif ( $func_id =~ /KEGG\_Category\_EC/ ) { 
                my ($id1, $id2) = split(/\:/, $func_id); 
                $db_id = $id2; 
                $sql = "select distinct g.gene_oid, kp.category " . 
                    "from gene_ko_enzymes g, image_roi_enzymes ire, " . 
                    "image_roi ir, kegg_pathway kp " . 
                    "where g.enzymes = ire.enzymes " . 
                    "and ire.roi_id = ir.roi_id " . 
                    "and ir.pathway = kp.pathway_oid " . 
                    "and kp.category = (select kp3.category from kegg_pathway kp3 " . 
                    "where kp3.pathway_oid = ?) "; 
            } 
            elsif ( $func_id =~ /KEGG\_Pathway\_EC/ ) { 
                my ($id1, $id2) = split(/\:/, $func_id); 
                $db_id = $id2; 
                $sql = "select g.gene_oid, ir.pathway " . 
                    "from gene_ko_enzymes g, image_roi_enzymes ire, " . 
                    "image_roi ir " . 
                    "where g.enzymes = ire.enzymes " . 
                    "and ire.roi_id = ir.roi_id " . 
                    "and ir.pathway = ? "; 
            } 
            elsif ( $func_id =~ /EC/ ) { 
                $sql = "select distinct g.gene_oid, g.enzymes " . 
                    "from gene_ko_enzymes g " . 
                    "where g.enzymes = ?"; 
            } 
            elsif ( $func_id =~ /^MetaCyc/ ) { 
                my ($id1, $id2) = split(/\:/, $func_id); 
                $db_id = $id2; 
                $sql = "select distinct g.gene_oid, brp.in_pwys " .
		    "from biocyc_reaction_in_pwys brp, gene_biocyc_rxns g " .
		    "where brp.unique_id = g.biocyc_rxn " .
		    "and brp.in_pwys = ? ";
            } 
            elsif ( $func_id =~ /^IPR/ ) { 
                $sql = "select distinct g.gene_oid, g.iprid " .
		    "from gene_img_interpro_hits g " .
		    "where g.iprid = ? ";
            } 
            elsif ( $func_id =~ /^TC/ ) { 
                $sql = "select distinct g.gene_oid, g.tc_family " . 
                    "from gene_tc_families g " . 
                    "where g.tc_family = ? "; 
	    } 
            elsif ( $func_id =~ /^ITERM/ ) { 
                my ($id1, $id2) = split(/\:/, $func_id); 
                $db_id = $id2; 
                if ( isInt($id2) ) { 
                    $sql = "select distinct g.gene_oid, g.function " .
                        "from gene_img_functions g " .
                        "where g.function = ? "; 
                } 
            } 
            elsif ( $func_id =~ /^IPWAY/ ) { 
                my ($id1, $id2) = split(/\:/, $func_id); 
                $db_id = $id2; 
                if ( isInt($id2) ) { 
		    $sql = "select g.gene_oid, ipr.pathway_oid " .
			"from img_pathway_reactions ipr, " .
			"img_reaction_catalysts irc, " .
			"gene_img_functions g " .
			"where ipr.pathway_oid = ? " .
			"and ipr.rxn = irc.rxn_oid " .
			"and irc.catalysts = g.function " .
			"and g.gene_oid between $min_gene_oid " .
			"and $max_gene_oid " .
			"union ".
			"select g2.gene_oid, ipr2.pathway_oid " .
			"from img_pathway_reactions ipr2, " .
			"img_reaction_t_components irtc, " .
			"gene_img_functions g2 " .
			"where ipr2.pathway_oid = ? " .
			"and ipr2.rxn = irtc.rxn_oid " .
			"and irtc.term = g2.function " .
			"and g2.gene_oid between $min_gene_oid " .
			"and $max_gene_oid ";
                } 
            } 

            if ($sql) { 
		my $cur;
                if ( $func_id =~ /IPWAY/ ) { 
#		    print "SQL: $sql ($db_id)\n";
		    $cur = $dbh->prepare( $sql ) || return;
		    $cur->execute( $db_id, $db_id );
                } 
                else { 
                    $sql .= " and g.gene_oid between $min_gene_oid and $max_gene_oid "; 
#		    print "SQL: $sql ($db_id)\n";
		    $cur = $dbh->prepare( $sql ) || return;
                    $cur->execute ( $db_id ); 
                } 
 
                for ( ; ; ) { 
                    my ( $gene_oid, $func_id2 ) = $cur->fetchrow(); 
                    last if ( !$gene_oid ); 
 
		    if ( defined($gene_href->{$gene_oid}) ) {
			# in gene set
		    }
		    else {
			next;
		    }

                    if ( $gene_href->{$gene_oid} ) { 
                        # already included
                        next; 
                    } 
                    $gene_href->{$gene_oid} = 1; 
 
                    if ( $h_ref->{$gene_oid} ) { 
                        if ($res) { 
			    my $gene_prod_name = "hypothetical protein";
			    if ( $gene_name_h{$gene_oid} ) {
				$gene_prod_name = $gene_name_h{$gene_oid};
			    }
			    else {
				my $sql2 = "select gene_display_name from gene where gene_oid = ?";
				my $cur2 = $dbh->prepare( $sql2 ) || return;
				$cur2->execute ( $gene_oid ); 
				($gene_prod_name) = $cur2->fetchrow();
				$cur2->finish();
				$gene_name_h{$gene_oid} = $gene_prod_name;
			    }
                            print $res "$gene_set\t$func_id\t$gene_oid\t$gene_prod_name\n";
                        }

                        $gene_count++;
                    } 
                } 
                $cur->finish(); 
            } 
        } 
        else { 
            my ( $taxon_oid, $data_type ) = split( /\|/, $key ); 
            my @func_genes = getFuncMetaGenes( $dbh, $func_id, $taxon_oid, $data_type ); 
 
            for my $gene_oid (@func_genes) {
                if ( $h_ref->{$gene_oid} ) {
                    my $r; 
                    my $workspace_id = "$taxon_oid $data_type $gene_oid";
 
                    if ( $gene_href->{$workspace_id} ) {
                        # already included
                        next; 
                    } 
                    $gene_href->{$workspace_id} = 1; 
 
                    if ($res) {
			my $gene_prod_name = "hypothetical protein";
			if ( $gene_name_h{$workspace_id} ) {
			    $gene_prod_name = $gene_name_h{$workspace_id};
			}
			else {
			    my $name2 = getGeneProdName($gene_oid, $taxon_oid, $data_type);
			    if ( $name2 ) {
				$gene_prod_name = $name2;
				$gene_name_h{$workspace_id} = $name2;
			    }
			}
			print $res "$gene_set\t$func_id\t$workspace_id\t$gene_prod_name\n";
                    } 

                    $gene_count++; 
                } 
            } 
        } 
    } 
 
    close FH; 
    return $gene_count;
} 


###########################################################################
# getFuncMetaGenes
###########################################################################
sub getFuncMetaGenes { 
    my ( $dbh, $func_id, $taxon_oid, $data_type ) = @_; 
 
    my %gene_h; 
    my @genes = (); 
 
    my $fname = ""; 
    my $file_name = ""; 

    my @func_list = (); 
 
    if ( $func_id =~ /COG\_Category/ || 
         $func_id =~ /COG\_Pathway/ ) { 
        my ($id1, $id2) = split(/\:/, $func_id); 
        my $sql = "select cf.cog_id from cog_functions cf where cf.functions = ?"; 
        if ( $func_id =~ /COG\_Pathway/ ) {
            $sql = "select cpcm.cog_members from cog_pathway_cog_members cpcm " . 
                "where cpcm.cog_pathway_oid = ?"; 
        } 

	my $cur = $dbh->prepare( $sql ) || return;
	$cur->execute ( $id2 ); 

        for ( ; ; ) { 
            my ($cog_id) = $cur->fetchrow(); 
            last if ! $cog_id;
            push @func_list, ( $cog_id );
        } 
        $cur->finish();
        $fname = "cog_genes";
    }
    elsif ( $func_id =~ /COG/ ) { 
        # COG
        @func_list = ( $func_id );
        $fname = "cog_genes";
    }
    elsif ( $func_id =~ /Pfam\_Category/ ) {
        my ($id1, $id2) = split(/\:/, $func_id);
        my $sql = "select pfc.ext_accession from pfam_family_cogs pfc " .
            "where pfc.functions = ?"; 
	my $cur = $dbh->prepare( $sql ) || return;
	$cur->execute ( $id2 ); 

        for ( ; ; ) { 
            my ($pfam_id) = $cur->fetchrow();
            last if ! $pfam_id; 
            push @func_list, ( $pfam_id ); 
        } 
        $cur->finish(); 

        $fname = "pfam_genes"; 
    } 
    elsif ( $func_id =~ /pfam/ ) { 
        # pfam
        @func_list = ( $func_id );
        $fname = "pfam_genes";
    }
    elsif ( $func_id =~ /TIGRfam\_Role/ ) {
        my ($id1, $id2) = split(/\:/, $func_id); 
        my $sql = "select tr.ext_accession from tigrfam_roles tr where tr.roles = ?";
	my $cur = $dbh->prepare( $sql ) || return;
	$cur->execute ( $id2 ); 

        for ( ; ; ) {
            my ($tigr_id) = $cur->fetchrow();
            last if ! $tigr_id; 
            push @func_list, ( $tigr_id );
        } 
        $cur->finish(); 
        $fname = "tigr_genes";
    } 
    elsif ( $func_id =~ /TIGR/ ) {
        # tigrfam
        @func_list = ( $func_id );
        $fname = "tigr_genes"; 
    } 
    elsif ( $func_id =~ /KEGG\_Category\_KO/ || 
            $func_id =~ /KEGG\_Pathway\_KO/ ) { 
        my ($id1, $id2) = split(/\:/, $func_id); 
        if ( $func_id =~ /KEGG\_Category\_KO/ ) { 
            my $sql2 = "select category from kegg_pathway where pathway_oid = ?"; 
	    my $cur2 = $dbh->prepare( $sql2 ) || return;
	    $cur2->execute( $id2 );

            ($id2) = $cur2->fetchrow(); 
            $cur2->finish(); 
        } 
        my $sql; 
        if ( $func_id =~ /KEGG\_Category\_KO/ ) { 
            $sql = "select distinct rk.ko_terms " . 
                "from image_roi_ko_terms rk, image_roi ir, kegg_pathway kp " . 
                "where ir.pathway = kp.pathway_oid and rk.roi_id = ir.roi_id " . 
                "and kp.category = ?"; 
        } 
        elsif ( $func_id =~ /KEGG\_Pathway\_KO/ ) {
            $sql = "select distinct rk.ko_terms " .
                "from image_roi_ko_terms rk, image_roi ir " .
                "where ir.pathway = ? and rk.roi_id = ir.roi_id";
        } 
	my $cur = $dbh->prepare( $sql ) || return;
	$cur->execute ( $id2 ); 

        for ( ; ; ) {
            my ($ec_id) = $cur->fetchrow();
            last if ! $ec_id; 
            push @func_list, ( $ec_id ); 
        } 
        $cur->finish(); 
        $fname = "ko_genes"; 
    } 
    elsif ( $func_id =~ /KO/ ) { 
        # ko
        @func_list = ( $func_id ); 
        $fname = "ko_genes"; 
    } 
    elsif ( $func_id =~ /KEGG\_Category\_EC/ || 
            $func_id =~ /KEGG\_Pathway\_EC/ ) { 
        my ($id1, $id2) = split(/\:/, $func_id); 
        if ( $func_id =~ /KEGG\_Category\_EC/ ) { 
            my $sql2 = "select category from kegg_pathway where pathway_oid = ?"; 
	    my $cur2 = $dbh->prepare( $sql2 ) || return;
	    $cur2->execute( $id2 );
            ($id2) = $cur2->fetchrow(); 
            $cur2->finish(); 
        } 
        my $sql = "select distinct ire.enzymes from image_roi_enzymes ire, image_roi ir " .
            "where ir.pathway = ? and ire.roi_id = ir.roi_id"; 
        if ( $func_id =~ /KEGG\_Category\_EC/ ) {
            $sql = "select distinct ire.enzymes " . 
                "from image_roi_enzymes ire, image_roi ir , kegg_pathway kp " .
                "where ir.pathway = kp.pathway_oid and ire.roi_id = ir.roi_id " .
                "and kp.category = ?";
        } 
 
	my $cur = $dbh->prepare( $sql ) || return;
	$cur->execute ( $id2 ); 

        for ( ; ; ) { 
            my ($ec_id) = $cur->fetchrow();
            last if ! $ec_id;
            push @func_list, ( $ec_id );
        } 
        $cur->finish(); 
        $fname = "ec_genes"; 
    } 
    elsif ( $func_id =~ /EC/ ) {
        # EC
        @func_list = ( $func_id );
        $fname = "ec_genes"; 
    } 
    elsif ( $func_id =~ /MetaCyc/ ) { 
        my ($id1, $id2) = split(/\:/, $func_id);
        my $sql2 = "select distinct br.ec_number " .
            "from biocyc_reaction br, biocyc_reaction_in_pwys brp " . 
            "where brp.unique_id = br.unique_id " . 
            "and brp.in_pwys = ? " . 
            "and br.ec_number is not null"; 
	my $cur2 = $dbh->prepare( $sql2 ) || return;
	$cur2->execute( $id2 );

        for ( ; ; ) { 
            my ($ec_id) = $cur2->fetchrow();
            last if ! $ec_id;
            push @func_list, ( $ec_id );
        } 
        $cur2->finish();
        $fname = "ec_genes"; 
    } 
    else {
        return @genes; 
    } 
 
    # check db first
    my $db_file_name = $mer_data_dir . "/" .
        $taxon_oid . "/" . $data_type . "/" . $fname . ".db"; 
    if ( -e $db_file_name ) { 
        my %h; 
        tie %h, "BerkeleyDB::Hash",
        -Filename => $db_file_name,
        -Flags    => DB_RDONLY,
        -Property  => DB_DUP; 
 
        if ( tied(%h) ) { 
            for my $id2 ( @func_list ) { 
                if ( $h{$id2} ) { 
                    my @gene_list = split(/\t/, $h{$id2});
                    for my $gene_oid ( @gene_list ) {
                        $gene_h{$gene_oid} = 1;
                    } 
                } 
            } 
 
            untie %h; 
            @genes = (keys %gene_h);
            return @genes;
        } 
    } 
 
    # else, use zip
    $file_name = $mer_data_dir . "/" .
        $taxon_oid . "/" . $data_type . "/" . $fname . ".zip";
    if ( ! $file_name || ! (-e $file_name) ) {
        return @genes; 
    } 
 
    for my $id3 ( @func_list ) { 
        if ( $id3 =~ /COG/ || $id3 =~ /pfam/ || $id3 =~ /TIGR/ ) {
            # $id3 = sanitizeGeneId3($id3);
        } 
        elsif ( $id3 =~ /KO/ ) {
            # ko
            my ($ko1, $ko2) = split(/\:/, $id3);
            $id3 = "KO:" . $ko2;
        } 
        elsif ( $id3 =~ /EC/ ) { 
            # EC
            my ($ec1, $ec2) = split(/\:/, $id3);
            $id3 = "EC:" . $ec2;
        } 
 
        my $cmd1 = "/usr/bin/unzip -p $file_name $id3 |";
        my $fh = new FileHandle($cmd1);
        if( !$fh ) {
            next;
        }
 
        my $line = ""; 
        while ( $line = $fh->getline() ) { 
            chomp($line); 
            my ($id1, $id2) = split(/\t/, $line);
            my $gene_oid = $id2;
            if ( ! $gene_oid ) {
                $gene_oid = $id1;
            }
 
            $gene_h{$gene_oid} = 1; 
        } 
 
        close $fh; 
    } 
 
    @genes = (keys %gene_h); 
    return @genes; 
} 
 
###########################################################################
# getGeneProdName
###########################################################################
sub getGeneProdName { 
    my ( $gene_oid, $taxon_oid, $data_type ) = @_; 
 
    # read gene fna info                                                                          
    my $t2 = 'unassembled'; 
    if ( $data_type eq 'assembled' ) { 
        $t2 = 'assembled'; 
    } 
 
    my $hash_file = $mer_data_dir . "/$taxon_oid" .
        "/" . $t2 . "/taxon_hash.txt";
    my $code = get_hash_code($hash_file, "gene_product", $gene_oid);
 
    my $zip_name = $mer_data_dir . "/$taxon_oid" .
        "/" . $t2 . "/gene_product";
    if ( $code ) { 
        $zip_name .= "/gene_product_" . $code; 
    } 
    $zip_name .= ".zip"; 
 
    if ( ! (-e $zip_name) ) { 
        return ""; 
    } 

    my $i2 = $gene_oid; 
    $i2 =~ s/\:/\_/g; 
    $i2 =~ s/\//\_/g; 
    my $cmd1 = "/usr/bin/unzip -p $zip_name $i2 |";
    my $fh = new FileHandle( $cmd1 );
    my $line = $fh->getline();
    chomp($line);
    my ($g3, $prod_name, $source) = split(/\t/, $line);
    close $fh; 
 
    if ( blankStr($prod_name) ) { 
        $prod_name = "hypothetical protein";
    } 
    return $prod_name;
} 


###########################################################################
# getFuncTypeIdSql
###########################################################################
sub getFuncTypeIdSql {
    my ( $functype ) = @_;

    my $sql = "";

    if ( $functype eq 'COG' ) {
	$sql = "select cog_id, cog_name from cog";
    }
    elsif ( $functype eq 'Pfam' ) {
	$sql = "select ext_accession, description from pfam_family";
    }
    elsif ( $functype eq 'TIGRfam' ) {
	$sql = "select ext_accession, expanded_name from tigrfam";
    }
    elsif ( $functype eq 'KO' ) {
	$sql = "select ko_id, ko_name from ko_term";
    }
    elsif ( $functype eq 'EC' || $functype eq 'Enzymes' ) {
	$sql = "select ec_number, enzyme_name from enzyme";
    }
    elsif ( $functype eq 'COG_Category' ) {
	$sql = "select function_code, definition from cog_function";
    }
    elsif ( $functype eq 'COG_Pathway' ) {
	$sql = "select cp.cog_pathway_oid, cog_pathway_name from cog_pathway cp";
    }
    elsif ( $functype eq 'Pfam_Category' ) {
	$sql = "select distinct cf.function_code, definition from cog_function cf";
    }
    elsif ( $functype eq 'KEGG_Category_EC' ) {
	$sql = qq{
            select distinct kp3.min_pid, kp.category, ire.enzymes
            from kegg_pathway kp, image_roi_enzymes ire, image_roi ir,
            (select kp2.category category, min(kp2.pathway_oid) min_pid
             from kegg_pathway kp2
             where kp2.category is not null
             group by kp2.category) kp3
            where ire.roi_id = ir.roi_id 
            and kp.pathway_oid = ir.pathway 
            and kp.category is not null
            and kp.category = kp3.category
        };
    }
    elsif ( $functype eq 'KEGG_Category_KO' ) {
	$sql = qq{
            select distinct kp3.min_pid, kp.category, rk.ko_terms
            from kegg_pathway kp, image_roi_ko_terms rk, image_roi ir,
            (select kp2.category category, min(kp2.pathway_oid) min_pid
             from kegg_pathway kp2
             where kp2.category is not null
             group by kp2.category) kp3
            where rk.roi_id = ir.roi_id 
            and kp.pathway_oid = ir.pathway 
            and kp.category is not null
            and kp.category = kp3.category
        };
    }
    elsif ( $functype eq 'KEGG_Pathway_EC' ) {
	$sql = "select distinct kp.pathway_oid, kp.pathway_name, ire.enzymes " .
	    "from kegg_pathway kp, image_roi_enzymes ire, image_roi ir " .
	    "where ire.roi_id = ir.roi_id and kp.pathway_oid = ir.pathway";
    }
    elsif ( $functype eq 'KEGG_Pathway_KO' ) {
	$sql = "select distinct kp.pathway_oid, kp.pathway_name, rk.ko_terms " .
	    "from kegg_pathway kp, image_roi_ko_terms rk, image_roi ir " .
	    "where rk.roi_id = ir.roi_id and kp.pathway_oid = ir.pathway";
    }
    elsif ( $functype eq 'TIGRfam_Role' ) {
	$sql = "select distinct t.role_id, t.sub_role, tr.ext_accession " .
	    "from tigr_role t, tigrfam_roles tr " .
	    "where tr.roles = t.role_id and t.sub_role is not null " .
	    "and t.sub_role != 'Other'";
    }

    return $sql;
}

###########################################################################################
# scafFuncTypeProfile
###########################################################################################
sub scafFuncTypeProfile {
    my ($contact_oid, $out_name, $functype, $scaf_set) = @_;

    my $log_file_name = "$out_dir/$contact_oid/job/$out_name/log.txt";
    open (LOG_FILE, '>>', $log_file_name) or return;
    print LOG_FILE "scafFuncTypeProfile\n";

    if ( ! $functype ) {
	my $error_file_name = "$out_dir/$contact_oid/job/$out_name/error.txt";
	open (ERROR_FILE, '>', $error_file_name) or return;
	print ERROR_FILE "No function category\n";
	print ERROR_FILE currDateTime() . "\n";
	close ERROR_FILE;
	close LOG_FILE;
	return;
    }

    my @filenames = ();
    if ( $scaf_set ) {
	@filenames = split(/\,/, $scaf_set);
    }
    if ( scalar(@filenames) == 0 ) {
	my $error_file_name = "$out_dir/$contact_oid/job/$out_name/error.txt";
	open (ERROR_FILE, '>', $error_file_name) or return;
	print ERROR_FILE "No scaffold set\n";
	print ERROR_FILE currDateTime() . "\n";
	close ERROR_FILE;
	close LOG_FILE;
	return;
    }

    print LOG_FILE "Function Category: $functype\n";
    print LOG_FILE "Scaffold Set: $scaf_set\n";

    # get all functions
    print LOG_FILE "loading $functype functions ...\n";
    my $dbh = dbLogin(); 
    my $k = 0; 
    my @func_ids = (); 
    my $sql = getFuncTypeIdSql($functype); 
    if ( $sql ) { 
        my %h2;
        my $cur = $dbh->prepare( $sql );
        $cur->execute();
        for (;;) { 
            my ($id2, $name2, @rest2) = $cur->fetchrow();
            last if !$id2; 
 
            $h2{$id2} = $name2; 
	}

	for my $id2 (keys %h2) {
	    push @func_ids, ( $id2 );
	}
    }

    my $line = "";
    my $line_no = 0;

    my $out_file_name = "$out_dir/$contact_oid/job/$out_name/profile.txt";
    open (OUTPUT_FILE, '>', $out_file_name) or return;

    my $out_list_name = "$out_dir/$contact_oid/job/$out_name/list.txt";
    my $fh_list = new FileHandle($out_list_name, "w");

    for my $scaf_set1 ( @filenames ) {
	print LOG_FILE "Processing scaffold set $scaf_set1 ...\n";

	# read all gene oids
	my %gene_h;
	my @db_scaffolds = ();
	my $cnt1 = 0;
	my $sset = new FileHandle("$workspace_dir/$contact_oid/scaffold/$scaf_set1");
	while ( my $id = $sset->getline() ) {
	    chomp $id;
	    next if ( $id eq "" );

	    $cnt1++;
	    if ( ($cnt1 % 1000) == 0 ) {
		print LOG_FILE "$cnt1 scaffolds processed ...\n";
	    }

	    my @v = split( / /, $id );
	    my $scaffold_oid = 0;
	    my $data_type    = "database";
	    my $taxon_oid    = 0; 
 
	    if ( scalar(@v) >= 3 ) { 
		$taxon_oid    = $v[0];
		$data_type    = $v[1]; 
		$scaffold_oid = $v[2]; 
	    }
	    elsif ( scalar(@v) > 0 ) { 
		$scaffold_oid = $v[0];
	    } 
 
	    my $key = ""; 
	    if ( $data_type eq 'database' ) { 
		$key = $data_type; 
		push @db_scaffolds, ($scaffold_oid); 
	    } 
	    elsif ( $data_type eq 'assembled' || $data_type eq 'unassembled' ) { 
		$key = "$taxon_oid|$data_type"; 
		my @genes_on_s =
		    getScaffoldGenes( $taxon_oid, $data_type, $scaffold_oid );
		for my $gid ( @genes_on_s ) {
		    $gene_h{$gid} = 0;
		}
	    }
	}  # end while id
	print LOG_FILE "$cnt1 scaffolds processed ...\n";

	if ( scalar(@db_scaffolds) > 0 ) {
	    print LOG_FILE "querying database ...\n";

	    my $scaf_str   = ""; 
	    my $s_cnt      = 0; 
	    my $total_scaf = scalar(@db_scaffolds);
	    for my $scaffold_oid (@db_scaffolds) {
		if ($scaf_str) {
		    $scaf_str .= ", " . $scaffold_oid; 
		}
		else { 
		    $scaf_str = $scaffold_oid; 
		} 
		$s_cnt++; 
 
		if ( ( ( $s_cnt % 1000 ) == 0 ) || $s_cnt >= $total_scaf ) {
		    if ( !$scaf_str ) {
			last; 
		    } 
 
		    my $sql2 = "select gene_oid from gene where scaffold in (" .
			$scaf_str . ")";
		    my $cur2 = $dbh->prepare( $sql2 );
		    $cur2->execute ( );
		    for ( ; ; ) {
			my ( $gene_oid ) = $cur2->fetchrow();
			last if ( !$gene_oid );
 
			$gene_h{$gene_oid} = 0;
                    } 
		    $cur2->finish(); 
 
		    # reset
		    $scaf_str = ""; 
                } 
            } 
	}   # end if db_scaffolds

	for my $func_id ( sort @func_ids ) {
	    print LOG_FILE "computing function $func_id ...\n";

	    my $cnt = computeGeneFuncProfile($dbh, $fh_list, $func_id, $scaf_set1, \%gene_h);
	    print OUTPUT_FILE "$scaf_set1\t$func_id\t$cnt\n";

	    # clear gene_h
	    for my $gene_oid (keys %gene_h) {
		$gene_h{$gene_oid} = 0;
	    }
	}
    }

    close OUTPUT_FILE;
    $fh_list->close();
    $dbh->disconnect();

    my $done_file = "$out_dir/$contact_oid/job/$out_name/done.txt";
    open (DONE_FILE, '>', $done_file) or return;
    print DONE_FILE currDateTime() . "\n";
    close DONE_FILE;

    print LOG_FILE currDateTime() . "\n";

    my $i = 0; 

    close LOG_FILE;
}


###########################################################################################
# scafFuncSetProfile
###########################################################################################
sub scafFuncSetProfile {
    my ($contact_oid, $out_name, $func_set, $scaf_set) = @_;

    my $log_file_name = "$out_dir/$contact_oid/job/$out_name/log.txt";
    open (LOG_FILE, '>>', $log_file_name) or return;
    print "scafFuncSetProfile\n";

    if ( ! $func_set ) {
	my $error_file_name = "$out_dir/$contact_oid/job/$out_name/error.txt";
	open (ERROR_FILE, '>', $error_file_name) or return;
	print ERROR_FILE "No function set\n";
	print ERROR_FILE currDateTime() . "\n";
	close ERROR_FILE;
	close LOG_FILE;
	return;
    }

    my @filenames = ();
    if ( $scaf_set ) {
	@filenames = split(/\,/, $scaf_set);
    }
    if ( scalar(@filenames) == 0 ) {
	my $error_file_name = "$out_dir/$contact_oid/job/$out_name/error.txt";
	open (ERROR_FILE, '>', $error_file_name) or return;
	print ERROR_FILE "No scaffold set\n";
	print ERROR_FILE currDateTime() . "\n";
	close ERROR_FILE;
	close LOG_FILE;
	return;
    }

    # read function set
    my @func_ids = ();
    my $res = new FileHandle("$workspace_dir/$contact_oid/function/$func_set");
    while ( my $id = $res->getline() ) {
	chomp $id;
	next if ( $id eq "" );
	push @func_ids, ($id);
    } 
    close $res;

    print LOG_FILE "Function Set: $func_set\n";
    print LOG_FILE "Scaffold Set: $scaf_set\n";

    my $line = "";
    my $line_no = 0;

    my $out_file_name = "$out_dir/$contact_oid/job/$out_name/profile.txt";
    open (OUTPUT_FILE, '>', $out_file_name) or return;

    my $out_list_name = "$out_dir/$contact_oid/job/$out_name/list.txt";
    my $fh_list = new FileHandle($out_list_name, "w");

    my $dbh = dbLogin();
    for my $scaf_set1 ( @filenames ) {
	print LOG_FILE "Processing scaffold set $scaf_set1 ...\n";

	# read all gene oids
	my %gene_h;
	my @db_scaffolds = ();
	my $sset = new FileHandle("$workspace_dir/$contact_oid/scaffold/$scaf_set1");
	while ( my $id = $sset->getline() ) {
	    chomp $id;
	    next if ( $id eq "" );

	    my @v = split( / /, $id );
	    my $scaffold_oid = 0;
	    my $data_type    = "database";
	    my $taxon_oid    = 0; 
 
	    if ( scalar(@v) >= 3 ) { 
		$taxon_oid    = $v[0];
		$data_type    = $v[1]; 
		$scaffold_oid = $v[2]; 
	    }
	    elsif ( scalar(@v) > 0 ) { 
		$scaffold_oid = $v[0];
	    } 
 
	    my $key = ""; 
	    if ( $data_type eq 'database' ) { 
		$key = $data_type; 
		push @db_scaffolds, ($scaffold_oid); 
	    } 
	    elsif ( $data_type eq 'assembled' || $data_type eq 'unassembled' ) { 
		$key = "$taxon_oid|$data_type"; 
		my @genes_on_s = 
		    getScaffoldGenes( $taxon_oid, $data_type, $scaffold_oid );
		for my $gid ( @genes_on_s ) {
		    $gene_h{$gid} = 0;
		}
	    }
	}

	if ( scalar(@db_scaffolds) > 0 ) {
	    my $scaf_str   = ""; 
	    my $s_cnt      = 0; 
	    my $total_scaf = scalar(@db_scaffolds);
	    for my $scaffold_oid (@db_scaffolds) {
		if ($scaf_str) {
		    $scaf_str .= ", " . $scaffold_oid; 
		}
		else { 
		    $scaf_str = $scaffold_oid; 
		} 
		$s_cnt++; 
 
		if ( ( ( $s_cnt % 1000 ) == 0 ) || $s_cnt >= $total_scaf ) {
		    if ( !$scaf_str ) {
			last; 
		    } 
 
		    my $sql2 = "select gene_oid from gene where scaffold in (" .
			$scaf_str . ")";
		    my $cur2 = $dbh->prepare( $sql2 );
		    $cur2->execute ( );
		    for ( ; ; ) {
			my ( $gene_oid ) = $cur2->fetchrow();
			last if ( !$gene_oid );
 
			$gene_h{$gene_oid} = 0;
                    } 
		    $cur2->finish(); 
 
		    # reset
		    $scaf_str = ""; 
                } 
            } 
	}   # end if db_scaffolds

	for my $func_id ( sort @func_ids ) {
	    my $cnt = computeGeneFuncProfile($dbh, $fh_list, $func_id, $scaf_set1, \%gene_h);
	    print OUTPUT_FILE "$scaf_set1\t$func_id\t$cnt\n";

	    # clear gene_h
	    for my $gene_oid (keys %gene_h) {
		$gene_h{$gene_oid} = 0;
	    }
	}
    }

    close OUTPUT_FILE;
    $fh_list->close();
    $dbh->disconnect();

    my $done_file = "$out_dir/$contact_oid/job/$out_name/done.txt";
    open (DONE_FILE, '>', $done_file) or return;
    print DONE_FILE currDateTime() . "\n";
    close DONE_FILE;

    print LOG_FILE currDateTime() . "\n";

    my $i = 0; 

    close LOG_FILE;
}


###########################################################################################
# scafPhyloDist
###########################################################################################
sub scafPhyloDist {
    my ($contact_oid, $out_name, $scaf_set) = @_;

    my $log_file_name = "$out_dir/$contact_oid/job/$out_name/log.txt";
    open (LOG_FILE, '>>', $log_file_name) or return;
    print LOG_FILE "scafPhyloDist $out_name $scaf_set\n";

    my @filenames = ();
    if ( $scaf_set ) {
	@filenames = split(/\,/, $scaf_set);
    }
    if ( scalar(@filenames) == 0 ) {
	my $error_file_name = "$out_dir/$contact_oid/job/$out_name/error.txt";
	open (ERROR_FILE, '>', $error_file_name) or return;
	print ERROR_FILE "No scaffold set\n";
	print ERROR_FILE currDateTime() . "\n";
	close ERROR_FILE;
	close LOG_FILE;
	return;
    }

    print LOG_FILE "Scaffold Set: $scaf_set\n";

    my $line = "";
    my $line_no = 0;

    my $out_file_name = "$out_dir/$contact_oid/job/$out_name/profile.txt";
    open (OUTPUT_FILE, '>', $out_file_name) or return;

    my $out_list_name = "$out_dir/$contact_oid/job/$out_name/list.txt";
    my $fh_list = new FileHandle($out_list_name, "w");

    my %taxon_h;
    my %cnt30;
    my %cnt60;
    my %cnt90;
    my %depth30;
    my %depth60;
    my %depth90;

    my %gene_h;
    my @db_scaffolds = ();
    my %files_to_check;
    my $cnt1 = 0;

    my %gene_name_h;
    undef %gene_name_h;

    # read all scaffolds
    for my $scaf_set1 ( @filenames ) {
	print LOG_FILE "Processing scaffold set $scaf_set1 ...\n";

	# read all gene oids
	my $sset = new FileHandle("$workspace_dir/$contact_oid/scaffold/$scaf_set1");
	while ( my $id = $sset->getline() ) {
	    chomp $id;
	    next if ( $id eq "" );

	    $cnt1++;
	    if ( ($cnt1 % 1000) == 0 ) {
		print LOG_FILE "$cnt1 scaffolds processed ...\n";
	    }

	    my @v = split( / /, $id );
	    my $scaffold_oid = 0;
	    my $data_type    = "database";
	    my $taxon_oid    = 0; 
 
	    if ( scalar(@v) >= 3 ) { 
		$taxon_oid    = $v[0];
		$data_type    = $v[1]; 
		$scaffold_oid = $v[2]; 
	    }
	    elsif ( scalar(@v) > 0 ) { 
		$scaffold_oid = $v[0];
	    } 
 
	    my $key = ""; 
	    if ( $data_type eq 'database' ) { 
		$key = $data_type; 
		push @db_scaffolds, ($scaffold_oid); 
	    } 
	    elsif ( $data_type eq 'assembled' || $data_type eq 'unassembled' ) { 
		$key = "$taxon_oid|$data_type"; 
		$files_to_check{$key} = 1;
		my @genes_on_s =
		    getScaffoldGenes( $taxon_oid, $data_type, $scaffold_oid );
		for my $gid ( @genes_on_s ) {
		    print LOG_FILE "gene $gid\n";
		    $gene_h{$gid} = 1;
		}
	    }
	}  # end while id
	print LOG_FILE "$cnt1 scaffolds processed ...\n";
    }

    my $total_gene_count = 0;
    my $total_est_copy = 0;

    # process db scaffolds
    my %db_gene_h;
    if ( scalar(@db_scaffolds) > 0 ) {
	print LOG_FILE "querying database ...\n";

	my $dbh = dbLogin();
	my $scaf_str   = ""; 
	my $s_cnt      = 0; 
	my $total_scaf = scalar(@db_scaffolds);
	for my $scaffold_oid (@db_scaffolds) {
	    if ($scaf_str) {
		$scaf_str .= ", " . $scaffold_oid; 
	    }
	    else { 
		$scaf_str = $scaffold_oid; 
	    } 
	    $s_cnt++; 
 
	    if ( ( ( $s_cnt % 1000 ) == 0 ) || $s_cnt >= $total_scaf ) {
		if ( !$scaf_str ) {
		    last; 
		} 
 
		my $sql2 = "select g.gene_oid, g.gene_display_name, dt.percent_identity, " .
		    "dt.homolog, dt.homolog_taxon, s.read_depth " .
		    "from dt_phylum_dist_genes dt, scaffold s, gene g " .
		    "where dt.gene_oid = g.gene_oid " .
		    "and g.scaffold = s.scaffold_oid " .
		    "and s.scaffold_oid in (" .
		    $scaf_str . ")";
		my $cur2 = $dbh->prepare( $sql2 );
		$cur2->execute ( );
		for ( ; ; ) {
		    my ( $gene_oid, $gene_prod_name, $percent_identity, $homolog, $homo_taxon,
			 $read_depth) = $cur2->fetchrow();
		    last if ( !$gene_oid );
 
		    $db_gene_h{$gene_oid} = 1;

		    if ( ! $read_depth ) {
			$read_depth = 1;
		    }
		    $total_est_copy += $read_depth;
		    $total_gene_count += 1;

		    $gene_name_h{$gene_oid} = $gene_prod_name;
		    print $fh_list "$gene_oid\t$gene_prod_name\t$percent_identity\t$homolog\t" .
			"$homo_taxon\t$read_depth\n";

		    $taxon_h{$homo_taxon} = 1;
		    if ( $percent_identity >= 90 ) {
			if ( $cnt90{$homo_taxon} ) {
			    $cnt90{$homo_taxon} += 1;
			}
			else {
			    $cnt90{$homo_taxon} = 1;
			}

			if ( $depth90{$homo_taxon} ) {
			    $depth90{$homo_taxon} += $read_depth;
			}
			else {
			    $depth90{$homo_taxon} = $read_depth;
			}
		    }
		    elsif ( $percent_identity >= 60 ) {
			if ( $cnt60{$homo_taxon} ) {
			    $cnt60{$homo_taxon} += 1;
			}
			else {
			    $cnt60{$homo_taxon} = 1;
			}

			if ( $depth60{$homo_taxon} ) {
			    $depth60{$homo_taxon} += $read_depth;
			}
			else {
			    $depth60{$homo_taxon} = $read_depth;
			}
		    }
		    elsif ( $percent_identity >= 30 ) {
			if ( $cnt30{$homo_taxon} ) {
			    $cnt30{$homo_taxon} += 1;
			}
			else {
			    $cnt30{$homo_taxon} = 1;
			}

			if ( $depth30{$homo_taxon} ) {
			    $depth30{$homo_taxon} += $read_depth;
			}
			else {
			    $depth30{$homo_taxon} = $read_depth;
			}
		    }
		} 
		$cur2->finish(); 
 
		# reset
		$scaf_str = ""; 
	    } 
	} 

	$dbh->disconnect();
    }   # end if db_scaffolds

    ## process fs scaffolds
    for my $key (keys %files_to_check) {
	my ($taxon_oid, $data_type) = split(/\|/, $key);

	$taxon_oid = sanitizeInt($taxon_oid);
	my $t2 = 'assembled';
	if ( $data_type eq 'unassembled' ) {
	    $t2 = 'unassembled';
	}

	for my $name2 ('phyloGene30.zip', 'phyloGene60.zip', 'phyloGene90.zip') {
	    my $zip_name = $mer_data_dir . "/$taxon_oid" .
		"/" . $t2 . "/" . $name2;

	    if ( -e $zip_name ) {
		my $cmd1 = "/usr/bin/unzip -p $zip_name |";
		print LOG_FILE "$cmd1\n";
		my $fh = new FileHandle( $cmd1 );
		while (my $line = $fh->getline()) {
		    chomp($line);
		    my ($g3, $percent_identity, $homolog, $family, $genus, $species,
			$homo_taxon, $read_depth) = split(/\t/, $line);

		    my $workspace_id = "$taxon_oid $t2 $g3";
		    if ( ! $gene_h{$workspace_id} ) {
			next;
		    }

		    if ( ! $read_depth ) {
			$read_depth = 1;
		    }
		    $total_est_copy += $read_depth;
		    $total_gene_count += 1;

		    my $gene_prod_name = "hypothetical protein";
		    if ( $gene_name_h{$workspace_id} ) {
			$gene_prod_name = $gene_name_h{$workspace_id};
		    }
		    else {
			my $name2 = getGeneProdName($g3, $taxon_oid, $t2);
			if ( $name2 ) {
			    $gene_prod_name = $name2;
			    $gene_name_h{$workspace_id} = $name2;
			}
		    }

		    print $fh_list "$workspace_id\t$gene_prod_name\t$percent_identity\t$homolog\t" .
			"$homo_taxon\t$read_depth\n";

		    $taxon_h{$homo_taxon} = 1;
		    if ( $percent_identity >= 90 ) {
			if ( $cnt90{$homo_taxon} ) {
			    $cnt90{$homo_taxon} += 1;
			}
			else {
			    $cnt90{$homo_taxon} = 1;
			}

			if ( $depth90{$homo_taxon} ) {
			    $depth90{$homo_taxon} += $read_depth;
			}
			else {
			    $depth90{$homo_taxon} = $read_depth;
			}
		    }
		    elsif ( $percent_identity >= 60 ) {
			if ( $cnt60{$homo_taxon} ) {
			    $cnt60{$homo_taxon} += 1;
			}
			else {
			    $cnt60{$homo_taxon} = 1;
			}

			if ( $depth60{$homo_taxon} ) {
			    $depth60{$homo_taxon} += $read_depth;
			}
			else {
			    $depth60{$homo_taxon} = $read_depth;
			}
		    }
		    elsif ( $percent_identity >= 30 ) {
			if ( $cnt30{$homo_taxon} ) {
			    $cnt30{$homo_taxon} += 1;
			}
			else {
			    $cnt30{$homo_taxon} = 1;
			}

			if ( $depth30{$homo_taxon} ) {
			    $depth30{$homo_taxon} += $read_depth;
			}
			else {
			    $depth30{$homo_taxon} = $read_depth;
			}
		    }
		}
	    }
	    else {
		print LOG_FILE "cannot find $zip_name\n";
	    }
	}
    }

    print OUTPUT_FILE "TotalGeneCount\t$total_gene_count\n";
    print OUTPUT_FILE "TotalEstCopy\t$total_est_copy\n";

    my @tids = sort { $a <=> $b } (keys %taxon_h);
    for my $taxon_oid2 (@tids) {
	print OUTPUT_FILE "$taxon_oid2\t" . 
	    $cnt30{$taxon_oid2} . "\t" . $cnt60{$taxon_oid2} . "\t" .
	    $cnt90{$taxon_oid2} . "\t" . $depth30{$taxon_oid2} . "\t" .
	    $depth60{$taxon_oid2} . "\t" . $depth90{$taxon_oid2} . "\n";
    }

    close OUTPUT_FILE;
    $fh_list->close();

    my $done_file = "$out_dir/$contact_oid/job/$out_name/done.txt";
    open (DONE_FILE, '>', $done_file) or return;
    print DONE_FILE currDateTime() . "\n";
    close DONE_FILE;

    print LOG_FILE currDateTime() . "\n";

    close LOG_FILE;
}


###########################################################################
# blankStr - Is blank string.  Return 1=true or 0=false.
###########################################################################   
sub blankStr { 
    my $s = shift;
 
    if ($s =~ /^[ \t\n]+$/ || $s eq "") {
        return 1; 
    } 
    else {
        return 0;
    } 
} 
 
#############################################################################
# isInt - Is integer.                                                        
############################################################################# 
sub isInt { 
    my $s = shift;
 
    if( $s =~ /^\-{0,1}[0-9]+$/) { 
        return 1;
    } 
    elsif( $s =~ /^\+{0,1}[0-9]+$/) {
        return 1;
    } 
    else { 
        return 0;
    }
}


#############################################################################
# strTrim
#############################################################################
sub strTrim { 
    my $s = shift; 
 
    $s =~ s/^\s+//; 
    $s =~ s/\s+$//; 
    return $s; 
} 


#############################################################################
# getUnzipFileName
#############################################################################
sub getUnzipFileName {
    my $s = shift;

    $s = strTrim($s);
    my ($size, $date1, $time1, $name1) = split(/[ \t\s]+/, $s, 4);

    return $name1;
}

#############################################################################
# currDateTime
#############################################################################
sub currDateTime {
    my $s = sprintf( 
                     "%d/%d/%d %d:%d:%d",
                     localtime->mon() + 1,     localtime->mday(),
                     localtime->year() + 1900, localtime->hour(), 
                     localtime->min(),         localtime->sec() 
	); 
    return $s; 
} 

#############################################################################
# checkPath
#############################################################################
sub checkPath { 
    my ($path) = @_; 

    ## Catch bad pattern first.
    my @toks = split( /\//, $path ); 
    for my $t (@toks) { 
        next if $t eq "";    # for double slashes
        if ( $t !~ /^[a-zA-Z0-9_\.\-\~]+$/ || $t eq ".." ) { 
	    return;
        } 
    } 
    ## Untaint.         
    $path =~ /([a-zA-Z0-9\_\.\-\/]+)/; 
    my $path2 = $1; 
    if ( $path2 eq "" ) { 
	return;
    } 
    return $path2; 
} 


############################################################################
# getScaffoldGenes 
############################################################################
sub getScaffoldGenes { 
    my ($taxon_oid, $data_type, $scaffold_oid) = @_; 
 
    my @genes_on_s = (); 
 
    # read scaffold info from file
    $taxon_oid = sanitizeInt($taxon_oid); 
    my $t2 = 'assembled'; 
    if ( $data_type eq 'unassembled' ) { 
        $t2 = 'unassembled'; 
    }
    my $hash_file = $mer_data_dir . "/$taxon_oid" .
        "/$t2/taxon_hash.txt";
    my $code = get_hash_code($hash_file, "scaffold_genes", $scaffold_oid);
    $code =  sanitizeGeneId3($code);
 
    my $zip_name = $mer_data_dir . "/$taxon_oid" . 
        "/$t2/scaffold_genes"; 
    if ( $code ) { 
        $zip_name .= "/scaffold_genes_" . $code; 
    } 
    $zip_name .= ".zip";
    if ( ! (-e $zip_name) ) {
        return @genes_on_s;
    } 
 
    unsetEnvPath(); 
 
    my $i2 = $scaffold_oid; 
    $i2 =~ s/\:/\_/g;
    $i2 =~ s/\//\_/g; 
    $i2 =  sanitizeGeneId3($i2);
 
#    my $cmd = "/usr/bin/unzip -C -p $zip_name $i2 |";
    my $cmd = "/usr/bin/unzip -p $zip_name $i2 |";
    my $fh = new FileHandle ($cmd);
    my $line = ""; 
    while ( $line = $fh->getline() ) {
        chomp($line);
	my ($gene_oid, @rest) = split(/\t/, $line);
	my $workspace_id = "$taxon_oid $data_type $gene_oid";
        push ( @genes_on_s, $workspace_id ); 
    } 
    close $fh; 
 
    resetEnvPath(); 
 
    return @genes_on_s;
} 

############################################################################ 
# sanitizeGeneId3 - Sanitize to integer, char and _ for security purposes. 
############################################################################ 
sub sanitizeGeneId3 { 
    my( $s ) = @_; 
    if ( ! $s ) {
	return $s;
    }

    if( $s !~ /^[0-9A-Za-z\_\.\-]+$/ ) { 
	return "";
    } 
    $s =~ /([0-9A-Za-z\_\.\-]+)/; 
    $s = $1; 
    return $s; 
} 



1;

