#!/bin/sh
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) 2012 NERSC, LBL

##
## 1. Check how many workers running now with <worker process name> 
## 2. If the number of workers are smaller than <min num workers>, start more workers
## 3. Check how many tasks are queued in the task_queue. If there are smaller number of
##    workers are runnning, add more workers with NTPW parameter
## 4. If need to start more workers, execute "start_worker.sh" which starts "self-terminated
##    worker processes". After <max lifespan> seconds, each process is checked it's status.
##    If it is idle, the "self-terminated worker process" is killed if it is idle.
##

module load python

#if [[ -z $1 || -z $2 || -z $3 || -z $4 ]] ; then
if [[ -z $1 || -z $2 ]] ; then
    echo "Usage: check_running_process.sh <worker process name> <min num workers> <NTPW> <max lifespan>"
    echo "       - worker process name: the worker process name. eq) img_worker.py"
    echo "       - min num workers: the min number of workers running"
    #echo "       - NTPW: number of tasks per worker"
    #echo "       - max lifespan: process lifespan in seconds "      
    exit 1
fi

##
## Get the number of running worker processes
##
SERVICE=$1
NPROCS2=`ps ax | grep -v grep | grep $SERVICE | wc -l`
NPROCS=$(($NPROCS2 - 2))
echo "Number of current running worker processes = $(($NPROCS))"

##
## Add more workers if the total number of running workers < MAXNUMPROCS
##
MAXNUMPROCS=$2
echo "The current setting for the default number of workers = $MAXNUMPROCS."
if [ $NPROCS -ge $MAXNUMPROCS ] ; then
    echo "More than $MAXNUMPROCS workers are running now."
else
    diff=$(($MAXNUMPROCS - $NPROCS))
    echo "Need to start $diff workers more."
    i="0"
    while [ $i -lt $diff ] ; do 
        echo "Start one more worker process..."
        ##
        ## This is to start the IMG precomputed database search worker
        ##
        ########################
        #python worker.py & 
        python ./attic/sulsj_dummy.py &
        ########################
        retcode=$?
        if [ $retcode -ne 0 ]; then
            echo "Couldn't start the worker process."
            exit $retcode
        fi
        i=$(($i+1))
        NPROCS=$(($NPROCS+1))
        echo "Total number of workers running = $NPROCS"
    done 
fi

###########################################################################
sleep 1

##
## Get the total number of tasks in the queue
## If too many tasks, add more workers.
##
################################################
NTASKS=`python check_num_tasks.py -q task_queue`
################################################
NTASKS=4
echo "Total number of tasks in the task queue = $NTASKS"

##
## THRESHOLD
## NUM tasks per worker
## ex) If NTPW=1, the number of new workers which will be started is same with
## the number of waiting jobs in the task queue.
## If NTPW=2, the number of new workers to add is half of the number of waiting jobs
##
#NTPW=$3

## UPDATE
NTPW=1 ## We're going to submit n jobs to the cluster, where n = number of jobs waiting in the queue

##
## Num tasks in the task queue waiting for a worker
##
NWAITING=$(($NTASKS - $NPROCS)) 
if [ $NWAITING -le 0 ] ; then
    echo "There are enough number of workers running now."
else
    NPROCS_NEEDED=$(($NWAITING / $NTPW))
    echo "Number of workers to add = $NPROCS_NEEDED"
    #i="0"
    #while [ $i -lt $NPROCS_NEEDED ] ; do
    #    ##
    #    ## Start self-terminated workers
    #    ## $4 is the lifespan (in seconds) of the worker
    #    ##
    #    ######################
    #    ./start_worker.sh $4 &
    #    ######################
    #    retcode=$?
    #    if [[ $retcode -ne 0 && $retcode -ne 21 ]]; then
    #        echo "Failed to add a new worker process."
    #        exit $retcode
    #    fi
    #    i=$(($i+1))
    #done
    
    ## UPDATE
    ## Need to check the array job is running using jobid stored in the JOBID file
    if [ -f ./JOBID ] ; then
        jobIdToCheck=`cat ./JOBID`
        echo "Array job ID = $jobIdToCheck"
        if [[ -n "$jobIdToCheck"  ]] ; then
            ret=`/usr/common/usg/bin/isjobcomplete $jobIdToCheck`
            echo "/usr/common/usg/bin/isjobcomplete $jobIdToCheck ==> $ret"
            if `echo ${ret} | grep "not" 1>/dev/null 2>&1` ; then
                #ret=`qsub -t 1:10000 -tc $NWAITING -n img_worker python ./worker.py`
                ret=`qsub -t 1:10 -tc $NWAITING -N img_worker ./attic/sulsj_dummy.py`
                jobid=`echo $ret | cut -d' ' -f 3 | cut -d'.' -f 1`
                echo "qsub -t 1:10 -tc $NWAITING -N img_worker ./attic/sulsj_dummy.py ==> $jobid"
                if [ $jobid -eq -1 ]; then
                    echo "Failed to submit the worker array job."
                    exit $?
                else
                    echo "You array-job is submitted. The jobid is stored in the JOBID file."
                    echo $jobid > JOBID
                fi
            else ## the array job is still runnnig
                cmd="qalter -tc $NWAITING $jobIdToCheck"
                $cmd
                echo "$cmd ==> $?"
                if [[ $? -ne 0 ]]; then
                    echo "Failed to run qalter to change the number of submitted/runnning jobs."
                    exit $?
                fi
            fi
        else
            ret=`qsub -t 1:10 -tc $NWAITING -N img_worker ./attic/sulsj_dummy.py`
            jobid=`echo $ret | cut -d' ' -f 3 | cut -d'.' -f 1`
            echo "qsub -t 1:10 -tc $NWAITING -N img_worker ./attic/sulsj_dummy.py ==> $jobid"
            if [[ $jobid -eq -1 ]]; then
                echo "Failed to submit the worker array job."
                exit $jobid
            else
                echo $jobid > JOBID
            fi
        fi
    else ## no JOBID file. Qsub new arra-job and save the jobid in JOBID
        ret=`qsub -t 1:10 -tc $NWAITING -N img_worker ./attic/sulsj_dummy.py`
        jobid=`echo $ret | cut -d' ' -f 3 | cut -d'.' -f 1`
        echo "qsub -t 1:10 -tc $NWAITING -N img_worker ./attic/sulsj_dummy.py ==> $jobid"
        if [[ $jobid -eq -1 ]]; then
            echo "Failed to submit the worker array job."
            exit $jobid
        else
            echo "You array-job is submitted. The jobid is stored in the JOBID file."
            echo $jobid > JOBID
        fi
    fi
fi

exit 0

## EOF

