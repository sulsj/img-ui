#! /usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) 2012 NERSC, LBL

"""


"""

import os
import sys
import time
import argparse
import subprocess
import logging
import datetime

try:
    import pika
except Exception, detail:
    print "Error importing pika. "
    print "Please check your installation."
    print detail
    sys.exit(1)
    
def _VmB(VmKey, pid):
    """
    get various mem usage properties of process with id pid in MB
    """

    _proc_status = '/proc/%d/status' % pid

    _scale = {'kB': 1.0/1024.0, 'mB': 1.0,
              'KB': 1.0/1024.0, 'MB': 1.0}

     # get pseudo file  /proc/<pid>/status
    try:
        t = open(_proc_status)
        v = t.read()
        t.close()
    except:
        return 0.0  # non-Linux?
     # get VmKey line e.g. 'VmRSS:  9999  kB\n ...'
    i = v.index(VmKey)
    v = v[i:].split(None, 3)  # whitespace
    if len(v) < 3:
        return 0.0  # invalid format?
     # convert Vm value to bytes
    return float(v[1]) * _scale[v[2]]


def get_memory_usage(pid):
    """
    return memory usage in Mb.
    """

    return _VmB('VmSize:', pid)


def get_cpu_load(pid):
    """
    return cpu usage of process
    """

    command = "ps h -o pcpu -p %d" % (pid)

    try:
        ps_pseudofile = os.popen(command)
        info = ps_pseudofile.read()
        ps_pseudofile.close()

        cpu_load = float(info.strip())
    except Exception, detail:
        #print "getting cpu info failed:", detail
        cpu_load = float(0) 
        #cpu_load = "non-linux?"

    return cpu_load


def get_runtime(pid):
    """
    get the total runtime in sec with pid.
    """

    _proc_stat = '/proc/%d/stat' % pid

    try:
        _proc_stat_f = os.popen('grep btime /proc/stat | cut -d " " -f 2')
        boottime = _proc_stat_f.read()
        _proc_stat_f.close()
        boottime = int(boottime.strip())

        _sec_since_boot_f = os.popen('cat /proc/%d/stat | cut -d " " -f 22' % (pid))
        sec_since_boot = _sec_since_boot_f.read()
        _sec_since_boot_f.close()
        sec_since_boot = int(sec_since_boot.strip())
        p_seconds_since_boot = sec_since_boot / 100

        p_starttime = boottime + p_seconds_since_boot
        now = time.time()

        p_runtime = int(now - p_starttime) # unit in seconds will be enough

    except Exception, detail:
        print "getting runtime failed: ", detail
        boottime = "non-linux?"

    return p_runtime


def setup_custom_logger(name, logLevel):
    if True:
        date_string = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
        log_file_name = (os.getcwd() + "/worker_" + date_string + ".log")
        
        logger = logging.getLogger(name)
        formatter = logging.Formatter('%(asctime)s | %(module)s | %(levelname)s : %(message)s')
        
        numeric_level = getattr(logging, logLevel.upper(), None)
        if not isinstance(numeric_level, int):
            raise ValueError('Invalid log level: %s' % logLevel)
        
        logger.setLevel(numeric_level)
        
        streamLogger = logging.StreamHandler()
        streamLogger.setLevel(numeric_level)
        streamLogger.setFormatter(formatter)
        
        #logFilePath = log_file_name
        #fileLogger = logging.FileHandler(logFilePath)
        #fileLogger.setFormatter(formatter)
        #fileLogger.setLevel(logging.DEBUG)
    
        #logger.addHandler(fileLogger)
        logger.addHandler(streamLogger)

        #logger.info("Log file name: %s" % (log_file_name))

    return logger


def check_if_idle(argv):
    """
        To check if the 
    
    """
    cpu_thresh = float(argv[2]) ## cpu threshold
    cpu_load = 0.0
    cpu_load = get_cpu_load(int(argv[1])) ## process id
    print "current cpu load:", cpu_load
    if (cpu_load > cpu_thresh):
        print "alive"
        return 1 
    else:
        print "seems like dead. check again"
        time.sleep(60) ## sleep for checking again
        cpu_load = get_cpu_load(int(argv[1]))
        if (cpu_load < cpu_thresh):
            print "dead confirmed"
            return 0 


def run_something(cmd):
    logger.debug("ps command: %s" % (cmd))
    try:
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    except:
        logger.critical("Failed to run user command, %s" % (cmd))
        sys,exit(1)
        
    stdout_value = ""
    for line in p.stdout.readlines():
        stdout_value += line
    p.wait()

    return stdout_value

def run_something_bg(cmd):
    logger.debug("ps command: %s" % (cmd))
    try:
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    except:
        logger.critical("Failed to run user command, %s" % (cmd))
        sys,exit(1)
        
    return p.pid

def get_num_workers(pname):
    ##
    ## Get the number of running worker processes
    ##
    nw = 0
    cmd = "ps ax | grep -v grep | grep " + pname + " | wc -l"
    nw = run_something(cmd)
    try:
        return int(nw) - 1
    except:
        return -1


def on_request(ch, method, props, body):
    if  (g_tag_id == props.correlation_id):
        ## send ack
        ch.basic_ack(delivery_tag = method.delivery_tag)
        
        #print " [x] Received %r" % (zloads(body),)
        msg_unzipped = zloads(body)
        print msg_unzipped["data"]
        
    #ch.stop_consuming()        
        
        
def check_num_tasks(qname):
    logger.info("Coonecting RabbitMQ server...")

    ## localhost
    #conn = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    ## remotehost
    credentials = pika.PlainCredentials('img', 'img')
    parameters = pika.ConnectionParameters(credentials=credentials,
                                           host='128.55.57.16',
                                           virtual_host='img_ui')
    conn = pika.BlockingConnection(parameters)
    ch = conn.channel()
 
    ## Check if all searches are done with n
    ## Declare queue for checking "result.method.message_count"
    result = ch.queue_declare(queue=qname, durable=True)
    numTasksInTheTaskQueue = result.method.message_count
    logger.info("Total number of tasks: %d" % (numTasksInTheTaskQueue))
    
    conn.close()
    
    return numTasksInTheTaskQueue 
    

def main(args):
    ##
    ## Get the number of running worker processes
    ##
    nCurrNumWorkers = get_num_workers(args.process_name)
    logger.info("Number of current running worker processes = %s" % (nCurrNumWorkers))
    
    ##
    ## Add more workers if the total number of running workers < MAXNUMPROCS
    ##
    nMinWorkers = int(args.nworkers)
    diff = 0
    logger.info("The current setting for the default number of workers = %s" % (nMinWorkers))
    if (nCurrNumWorkers >= nMinWorkers):
        logger.info("Number of current running workers = %d" % (nCurrNumWorkers))
    else:
        diff = nMinWorkers - nCurrNumWorkers
        logger.info("Need to start %d workers more." % (diff))
    
    for i in range(diff):
        logger.info("Try to start one more worker...")
        cmd = "python sulsj_dummy.py &"
        ret = run_something_bg(cmd)
        logger.info("pid: " + str(ret))
        if ret:
            nCurrNumWorkers += 1
            logger.info("One more worker is started. Total %d workers are running now." % (nCurrNumWorkers))
        
    ###########################################################################
    #time.sleep(5)
    
    ##
    ## Get the total number of tasks in the queue
    ## If too many tasks, add more workers.
    ##
    nCurrTasks = check_num_tasks("task_queue")
    logger.info("Total number of tasks in the task queue = %d" % (nCurrTasks))
    nCurrTasks = 1

    ##
    ## THRESHOLD
    ## NUM tasks per worker
    ## ex) If NTPW=1, the number of new workers which will be started is same with
    ## the number of waiting jobs in the task queue.
    ## If NTPW=2, the number of new workers to add is half of the number of waiting jobs
    ##
    NTPW = args.ntpw

    ##
    ## Num tasks in the task queue waiting for a worker
    ##
    nWaiting = nCurrTasks - nCurrNumWorkers
    if (nWaiting <= 0):
        logger.info("There are enough number of workers running now.")
    else:
        nWorkersToAdd = nWaiting / NTPW + 1
        logger.info("Number of workers to add = %d" % (nWorkersToAdd))
        for i in range(nWorkersToAdd):
            pass

if __name__ == "__main__":
    
    desc = u'check_running_process'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-n', '--pname', help='worker process name: the worker process name. eq) img_worker.py', dest='process_name', required=True)
    parser.add_argument('-w', '--nworkers', help='min num workers: the min number of workers running', dest='nworkers', required=True)
    parser.add_argument('-t', '--ntpw', help='NTPW: number of tasks per worker', dest='ntpw', required=True)
    parser.add_argument('-d', '--life', help='max lifespan: process lifespan in seconds', dest='ntpw', required=True)
    parser.add_argument('-l', '--loglevel', help='loggin level', dest='logLevel', required=False, default="info")
    args = parser.parse_args()
    
    logger = setup_custom_logger('root', args.logLevel)
    sys.exit(main(args))


