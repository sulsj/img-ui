#!/bin/bash
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) 2012 NERSC, LBL

module load python

##
## This is the IMG precomputed database search worker
##
########################
python worker.py &
#python sulsj_dummy.py &
########################
IMGWorkerPID=$!
retcode=$?
if [ $retcode -ne 0 ]; then
    echo "Failed to start a new worker process."
    exit $retcode
fi

##
## This is the lifespan of the process. After this seconds, this process
## will be checked if it is idle or not. If idle, it will be killed.
##
########
sleep $1 
########

cpu_thresh=1.0 ## Threshold for determininng the worker process is idle or not
sleep_time=60  ## Duration for next idle checking

## Check if the worker is idle or not
## Arguments:
##    - $IMGWorkerPID: worker process id
##    - $cpu_thresh: threshold for determining idle process
##
#############################################################
while ! python check_if_alive.py $IMGWorkerPID $cpu_thresh
do
    sleep $sleep_time
done
#############################################################

echo "This IMG worker, pid=$IMGWorkerPID is idle. Try to remove it."
######################
kill -9 $IMGWorkerPID
######################
retcode=$?
if [ $retcode -ne 0 ]; then
    echo "Failed to kill the worker process or the process is already terminated."
    exit 21
fi

exit 0
 
## EOF