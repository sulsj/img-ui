#!/bin/bash 
export ORACLE_HOME=/jgi/tools/oracle_client/DEFAULT
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${ORACLE_HOME}/lib32
export PATH=$PATH:.:/usr/local/bin:/usr/bin:/bin:${ORACLE_HOME}/bin
export IMG_SCRIPTS_DIR=/project/projectdirs/genomes/sulsj/test/2012.08.15-img/amy-test/git-cloned/img-ui/crontab-scripts
