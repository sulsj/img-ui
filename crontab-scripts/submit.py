#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""

This is to submit a task to the search server with a list of oids.

Usage:

    client.py --oid <list of comma separated oids> [--oidfile <oid file name>] [--outfile <output file name>]

"""
import uuid
import getopt
import gzip
import cPickle
import zlib
import sys
import subprocess
import re
import os
import time

try:
    import pika
except Exception, detail:
    print "Error importing pika. "
    print "Please check your installation."
    print detail
    sys.exit(1)
    
    
def zdumps(obj):
    """
    dumps pickleable object into zlib compressed string
    """
    return zlib.compress(cPickle.dumps(obj,cPickle.HIGHEST_PROTOCOL),9)

    
def zloads(zstr):
    """
    loads pickleable object from zlib compressed string
    """
    return cPickle.loads(zlib.decompress(zstr))


def is_running(process):
    """
    Check "process" is runinng
    
    @param process: process name
    @type string
    """
    s = subprocess.Popen(["ps", "axw"],stdout=subprocess.PIPE)
    for x in s.stdout:
        if re.search(process, x):
            return True

    return False


def usage():
    print "Usage: client.py --oid <list of comma separated oids> [--oidfile <oid file name>] [--outfile <output file name>]"
    print "           ex) client.py --oid 1,2,3,4 --oidfile oids.txt"


def main(argv):
    """
    Parse the command line inputs and call send_info 
        
    @param argv: list of arguments
    @type argv: list of strings
    """
    
    oids = []
    outFileName = ""
    
    try:
        opts, args = getopt.getopt(argv[1:],"hf:o:i:",["help","oidfile=","output=","oid="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-i", "--oid"):
            oids = arg.split(',')
        elif opt in ("-f", "--oidfile"):
            try:
                #print "Loading oids form file,", arg 
                fh = open(arg, "r")
                for line in fh:
                    if len(line.strip()) > 0:
                        oids.append(line.strip())
                fh.close()
                #print "Total number of oids loaded: ", len(oids)
            except IOError:
                print >> sys.stderr, "ERROR: Cannot open oid file"
        elif opt in ("-o", "--output"):
            outputFileName = arg
        else:
            usage()
            sys.exit(2)
    
    if not len(oids) > 0:
        print "Please specify oid(s) to search"
        usage()
        sys.exit(1)
    else:
        print "Total number of oids: ", len(oids)
            
    ## localhost
    #connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    ## remotehost
    #credentials = pika.PlainCredentials('img', 'img')
    #parameters = pika.ConnectionParameters(credentials=credentials,
                                           #host='128.55.57.16',
                                           #virtual_host='img_ui')
    ## remote broker (carver)
#	credentials = pika.PlainCredentials('taskfarmer-mq', 'taskfarmer-mq')
#	parameters = pika.ConnectionParameters(credentials=credentials,
#                                           host='128.55.57.16',
#                                           virtual_host='taskfarmer-mq')
	## remote broker (mq.nersc.gov)
	credentials = pika.PlainCredentials('img', 'synchrotron')
	parameters = pika.ConnectionParameters(credentials=credentials,
                                           host='mq.nersc.gov',
                                           virtual_host='jgi')
    
    conn = pika.BlockingConnection(parameters)
    ch = conn.channel()
    
    ## Declare task sending queue (client --> worker)
    ch.queue_declare(queue='img_task_queue', durable=True)
    
    ## Declare result recv queue (worker --> client)
    #queue_name = str(uuid.uuid4())
    #result = ch.queue_declare(queue=queue_name, durable=False, exclusive=False, auto_delete=True)
    #callback_queue = result.method.queue
    #corr_id = str(uuid.uuid4())
    #print "Queue:  ", queue_name
    #print "Tag_id: ", corr_id
    
    for oid in oids:
        msgContainer = {}
        msgContainer["oid"] = oid
        msgContainer["flag"] = "assembled"
        #msgContainer["oid"] = 3300000030
        #msgContainer["flag"] = "unassembled"
        msgContainer["outfile"] = outFileName
        msgContainer["data"] = "" ## reserved for specifying the Perl module name to use
        msg_zipped = zdumps(msgContainer)
                
        ch.basic_qos(prefetch_count=1) ## how many messages send to server
        try:
            ch.basic_publish(exchange='',
                              routing_key='img_task_queue',
                              body=msg_zipped,
                              properties=pika.BasicProperties(
                                    delivery_mode = 2, ## make message persistent
                                    #reply_to = callback_queue, ## set returning queue name
                                    #correlation_id = corr_id,
                              ))
        except:
            print "Failed to submit a request"
            sys.exit(1)
        
    conn.close()


if __name__ == "__main__":

    ## Check if the worker is running.
    ## If not, start the worker process
    """
    worker_process_name = "worker.py"
    if not is_running(worker_process_name):
        print "The worker process is not running now. Please start the worker."
        #os.system("python "+ os.getcwd() + "/worker.py &")
        #time.sleep(3)
        #if not is_running(worker_process_name):
        #    print "Failed to start the worker process."
        sys.exit(1)
        #else:
        #    print "The worker is ready. Send the request to the worker."
    """
    
    sys.exit(main(sys.argv))
    
    
# EOF
