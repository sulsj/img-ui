#!/usr/bin/env python
import pika
import sys


"""
host = '128.55.57.16'
creds = pika.PlainCredentials('sulsj', 'changeme')
params = pika.ConnectionParameters(host, credentials=creds, heartbeat=False)
#conn = pika.AsyncoreConnection(params)

#params = pika.ConnectionParameters(host='localhost')


#credentials = pika.PlainCredentials('img', 'img')
#parameters = pika.ConnectionParameters(credentials=credentials,
#                                           host='128.55.57.16',
#                                           virtual_host='img_ui')
#conn = pika.BlockingConnection(parameters)


conn = pika.BlockingConnection(params)
ch = conn.channel()

try:
    ch.queue_delete(queue=sys.argv[1], nowait=True)
except:
    conn.close()

ch.close()
conn.close()
"""


if len(sys.argv) == 1:
    print "Usage: python delete_*.py <queue name>"
    sys.exit(1)

queueList = []
for line in open(sys.argv[1],'r'):
    toks = line.split("\t")
    for t in toks:
        if len(t.strip()) > 2:
            queueList.append(t.strip())
print queueList



for q in queueList:
    #host = '128.55.57.16'
    #creds = pika.PlainCredentials('taskfarmer-mq', 'taskfarmer-mq')
    #params = pika.ConnectionParameters(host, credentials=creds, virtual_host='taskfarmer-mq', heartbeat=True)
    ## localhost
    #connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    ## remotehost
    #credentials = pika.PlainCredentials('img', 'img')
    #parameters = pika.ConnectionParameters(credentials=credentials,
                                           #host='128.55.57.16',
                                           #virtual_host='img_ui')
    ## remote broker (carver)
    #credentials = pika.PlainCredentials('taskfarmer-mq', 'taskfarmer-mq')
    #parameters = pika.ConnectionParameters(credentials=credentials,
                                           #host='128.55.57.16',
                                           #virtual_host='taskfarmer-mq')
    ## remote broker (mq.nersc.gov)
    credentials = pika.PlainCredentials('sulsj', 'synchrotron')
    params = pika.ConnectionParameters(credentials=credentials,
                                        host='mq.nersc.gov',
                                        virtual_host='jgi')
    
    #conn = pika.AsyncoreConnection(params)
    conn = pika.BlockingConnection(params)
    ch = conn.channel()

    try:
        #ch.queue_delete(queue=sys.argv[1])
        ch.queue_delete(queue=str(q))
    except:
        print "Failed to queue_delete."
    
    print str(q), " was deleted."
    try:
        ch.close()
    except:
        pass
        #print "Failed to ch.close()"
    
    try:    
        conn.close()
    except:
        print "Failed to conn.close()"


