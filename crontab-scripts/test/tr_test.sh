#!/bin/bash

# create the string IP address
IPSTR="192.168.1.4"

# declare the array
declare -a IPARR

IPARR=(`echo $IPSTR | tr "." " "`)

# print out the first octet (should be 192). This actually prints out a blank line
echo ${IPARR[0]}

# also tried with quotes, but same result:
echo "${IPARR[0]}"


RET="Your job-array 242663.1-10:1 (img_worker) has been submitted"
IPARR=(`echo $RET | tr " " "\n"`)
echo ${IPARR[0]}  

