#!/bin/bash -l
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) 2012 NERSC, LBL

##
## 1. Check how many workers running now with <worker process name> 
## 2. If the number of workers are smaller than <min num workers>, start more workers
## 3. Check how many tasks are queued in the img_task_queue. If there are smaller number of
##    workers are runnning, add more workers. The number of workers to add is determined
##    based on the NTPW parameter
## 4. If need to start more workers, it does qsub to submit worker array jobs if there is
##    no such jobs queued/running. The "-tc" option will control the number of workers
##    If there is already a worker array job is queued/running, adjust the number of workers
##    by "-tc" option.
##

module load python

#PYTHONDIR=/usr/common/usg/languages/python/2.7.3_1
#READLINEDIR=/usr/common/usg/utility_libraries/readline/6.2
#export PATH=$PYTHONDIR/bin:$PATH
#export LD_LIBRARY_PATH=$PYTHONDIR/lib:$READLINEDIR/lib:$LD_LIBRARY_PATH
#export SGE_ROOT=/opt/uge/phoebe/uge

export ORACLE_HOME=/jgi/tools/oracle_client/DEFAULT
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${ORACLE_HOME}/lib32
export PATH=$PATH:.:/usr/local/bin:/usr/bin:/bin:${ORACLE_HOME}/bin
export IMG_SCRIPTS_DIR=/project/projectdirs/genomes/sulsj/test/2012.08.15-img/amy-test/git-cloned/img-ui/crontab-scripts/test-mq-nersc
export MYPYTHON=`which python`

##
## Submit array job
## $1: size of array job (should be arbitrarily large like 1000000)
## $2: number of workers to submit
##
function submit_arrayjob {
    ret=`/opt/uge/phoebe/uge/bin/lx-amd64/qsub -cwd -t 1:$1 -tc $2 -l h_rt=00:20:00 -N img_worker $IMG_SCRIPTS_DIR/run_img_worker.sh`
    echo $ret
    jobid=`echo $ret | cut -d' ' -f 3 | cut -d'.' -f 1`
    echo "qsub -cwd -t 1:$1 -tc $2 -l h_rt=00:20:00 -N img_worker $IMG_SCRIPTS_DIR/run_img_worker.sh ==> $jobid"
    if [[ $jobid -eq -1 || -z $jobid ]]; then
        echo "Failed to submit the worker array job."
        exit $jobid
    else
        echo "You array-job is submitted. The jobid is stored in the JOBID file."
        echo $jobid > $IMG_SCRIPTS_DIR/JOBID
        exit 0
    fi
}

##
## Submit n single job
## $1: number of workers to submit
##
function submit_singlejob {
    i="0"
    while [ $i -lt $1 ] ; do 
        ret=`/opt/uge/phoebe/uge/bin/lx-amd64/qsub -cwd -l h_rt=01:00:00 -N img_worker $IMG_SCRIPTS_DIR/run_img_worker.sh`
        echo $ret
        jobid=`echo $ret | cut -d' ' -f 3 | cut -d'.' -f 1`
        echo "qsub -cwd -l h_rt=01:00:00 -N img_worker $IMG_SCRIPTS_DIR/run_img_worker.sh ==> $jobid"
        if [[ $jobid -eq -1 || -z $jobid ]]; then
            echo "Failed to submit one of the worker jobs."
        else
            echo "Your job is submitted ==> jobid = $jobid"
        fi
        i=$(($i+1))
    done
}

if [[ -z $1 || -z $2 ]] ; then
    echo "Usage: check_running_process.sh <worker process name> <min num workers>"
    echo "       - worker process name: the worker process name. eq) img_worker.py"
    echo "       - min num workers needed: the min number of workers running"
    exit 1
fi

 
##
## Get the number of running worker processes
##
echo "======================= $(date) ========================"
WORKER_PROC_NAME=$1
NPROCS2=`ps ax | grep -v grep | grep -v check_running_img_worker.sh | grep $WORKER_PROC_NAME | wc -l`
NPROCS=$(($NPROCS2))
echo "Number of current running worker processes = $(($NPROCS))"

##
## Add more workers if the total number of running workers < MAXNUMPROCS
##
MAXNUMPROCS=$2
echo "The current setting for the default number of workers = $MAXNUMPROCS."
if [ $NPROCS -ge $MAXNUMPROCS ] ; then
    echo "More than or equal to $MAXNUMPROCS workers are running now."
else
    diff=$(($MAXNUMPROCS - $NPROCS))
    echo "Need to start $diff workers more."
    i="0"
    while [ $i -lt $diff ] ; do 
        echo "Start one more worker process..."
        ##
        ## This is to start the IMG precomputed database search worker
        ##
        ########################
        $IMG_SCRIPTS_DIR/worker.py & 
        ########################
        retcode=$?
        if [ $retcode -ne 0 ]; then
            echo "Couldn't start the worker process."
            echo $retcode
            exit 1
        else
            i=$(($i+1))
            NPROCS=$(($NPROCS+1))
            echo "Total number of workers running = $NPROCS"
        fi
    done 
fi

################################################################################
sleep 1

##
## Get the total number of tasks in the queue
## If too many tasks, add more workers.
##
########################################################
NTASKS=`$MYPYTHON $IMG_SCRIPTS_DIR/check_num_tasks_waiting.py -q img_task_queue`
########################################################
echo "Total number of tasks in the task queue = $NTASKS"

##
## THRESHOLD
## NUM tasks per worker
## ex) If NTPW=1, the number of new workers which will be started is same with
## the number of waiting jobs in the task queue.
## If NTPW=2, the number of new workers to add is half of the number of waiting jobs
##
## UPDATED
NTPW=1 ## We're going to submit n jobs to the cluster, where n = number of jobs waiting in the queue

##
## Num tasks in the task queue waiting for a worker
##
## NOTE: "qs" has "2min" delay than the real time qstat results
if [[ $NTASKS -gt 0 ]] ; then
    numBatchJobsQueued=`qs -s qw,Rq | grep img_work | wc -l`
    numBatchJobsRunning=`qs -s \*r | grep img_work | wc -l`
    echo "IMG worker processes running = $NPROCS"
    echo "IMG worker jobs queued = $numBatchJobsQueued"
    echo "IMG worker jobs running = $numBatchJobsRunning"
    NWAITING=$(($NTASKS - $NPROCS + $numBatchJobsQueued + $numBatchJobsRunning))
    
    if [ $NWAITING -le 0 ] ; then
        echo "There are enough number of workers running now."
    else
        NPROCS_NEEDED=$(($NWAITING / $NTPW))
        echo "Number of workers to add = $NPROCS_NEEDED"
        submit_singlejob $NPROCS_NEEDED
        exit $?
    fi
else
    echo "No need to add more worker jobs."
fi
exit 0

## EOF

