#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""

This is to run a search against the precomputed database using a list of oids.
Two output files will be saved in a location and the URL will be returned to
the queue. The user can come and check the result is ready to the queue using check.py.

"""

import sys
import gzip
import cPickle
import zlib
import subprocess
import time
import datetime
import logging
import os

try:
    import pika
except Exception, detail:
    print "Error importing pika. "
    print "Please check your installation."
    print detail
    sys.exit(1)
    
        
def zdumps(obj):
    """
    dumps pickleable object into zlib compressed string
    """
    return zlib.compress(cPickle.dumps(obj,cPickle.HIGHEST_PROTOCOL),9)

    
def zloads(zstr):
    """
    loads pickleable object from zlib compressed string
    """
    return cPickle.loads(zlib.decompress(zstr))


def run_something(msg_zipped):
    """
    Run kegg.pl with oid
    
    @param msg_zipped: compressed msg from client
    @type msg_zipped: dict with the keys, oid, flag, outfile, data
    """
    
    ##
    ## uncompress msg to get a task
    ##
    msg_unzipped = zloads(msg_zipped)
    oid = msg_unzipped["oid"]
    flag = msg_unzipped["flag"]
    outfile = msg_unzipped["outfile"]
    data = msg_unzipped["data"]
    
    ##
    ## Run the task
    ##
    logger.info( "Running search..." )
    perlCmd = "./kegg.pl "
    cmd = os.getcwd() + "/" + perlCmd + " " + str(oid) + " " + str(flag)
    #cmd = "uname -a  "
    try:
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    except:
        logger.critical("Failed to run user command, %s" % (cmd))
        
    logger.debug("User command: %s" % (cmd))
    stdout_value = ""
    for line in p.stdout.readlines():
        logger.info( line.strip() )
        stdout_value += line
    p.wait()
    
    logger.info( "Search completed." )
    
    ##
    ## Prepare result to send back
    ##
    msgContainer = {}
    msgContainer["oid"] = oid
    msgContainer["flag"] = flag
    msgContainer["data"] = stdout_value
    msg_zipped2 = zdumps(msgContainer)
    
    return msg_zipped2
    

def on_request(ch, method, props, body):
    ## send ack
    ch.basic_ack(delivery_tag = method.delivery_tag)
    
    msg_unzipped = zloads(body)
    logger.info( "Received %r" % (msg_unzipped,) )
    oid = msg_unzipped["oid"]
    flag = msg_unzipped["flag"]
    outfile = msg_unzipped["outfile"]
    data = msg_unzipped["data"]
            
    response = run_something(body)
    #ch.basic_publish(exchange='', # nameless exchange
    #                routing_key=props.reply_to, # use the queue which the client created
    #                properties=pika.BasicProperties(
    #                       delivery_mode = 2, # make message persistent
    #                       correlation_id = props.correlation_id
    #                ),
    #                body=response)
    
    #logger.info( "Send the result back with " )
    #logger.info("   Queue: %s" % (str(props.reply_to)))
    #logger.info("   Tag id: %s " % (str(props.correlation_id)))

    #time.sleep(5)
    #try:
    #    ch.stop_consuming() ## kill the worker
    #except:
    #    pass

def setup_custom_logger(name):
    if True:
        date_string = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

        logDir = os.getcwd() + "/log"
        if not os.path.exists(logDir):
            try:
                os.makedirs(logDir)
            except:
                print ("Failed to create dir, %s" % (logDir))
                sys.exit(1)
        log_file_name = (logDir + "/worker_" + date_string + ".log")
        
        logger = logging.getLogger(name)
        formatter = logging.Formatter('%(asctime)s | %(module)s | %(levelname)s : %(message)s')
        logger.setLevel(logging.DEBUG)
        
        streamLogger = logging.StreamHandler()
        streamLogger.setLevel(logging.WARNING)
        #streamLogger.setLevel(logging.DEBUG)
        streamLogger.setFormatter(formatter)
        
        logFilePath = log_file_name
        fileLogger = logging.FileHandler(logFilePath)
        fileLogger.setFormatter(formatter)
        fileLogger.setLevel(logging.DEBUG)
        
        logger.addHandler(fileLogger)
        logger.addHandler(streamLogger)

        logger.info("Log file name: %s" % (log_file_name))
    
    return logger


def main(argv):
    ## localhost
    #connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))

    ## remote broker (carver)
    #credentials = pika.PlainCredentials('taskfarmer-mq', 'taskfarmer-mq')
    #parameters = pika.ConnectionParameters(credentials=credentials,
    #                                       host='128.55.57.16',
    #                                       virtual_host='taskfarmer-mq')
    #credentials = pika.PlainCredentials('img', 'img')
    #parameters = pika.ConnectionParameters(credentials=credentials,
                                           #host='128.55.57.16',
                                           #virtual_host='img_ui')
    ## remote broker (mq.nersc.gov)
    credentials = pika.PlainCredentials('img', 'synchrotron')
    parameters = pika.ConnectionParameters(credentials=credentials,
                                           host='mq.nersc.gov',
                                           virtual_host='jgi')
    
    connection = pika.BlockingConnection(parameters)

    channel = connection.channel()
    
    channel.queue_declare(queue='img_task_queue', durable=True)
    logger.info( 'Waiting for request.' )
    
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(on_request, queue='img_task_queue')
    
    channel.start_consuming()


if __name__ == "__main__":
    logger = setup_custom_logger('root')
    sys.exit(main(sys.argv))
    
# EOF