#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""
Connect to the RabbitMQ message queue server and get the number of tasks
queued in the "img_task_queue"

"""

import uuid
import gzip
import cPickle
import zlib
import sys
import os
import argparse

try:
    import pika
except Exception, detail:
    print "Error importing pika. "
    print "Please check your installation."
    print detail
    sys.exit(1)
    
    
def on_request(ch, method, props, body):
    if  (g_tag_id == props.correlation_id):
        ## send ack
        ch.basic_ack(delivery_tag = method.delivery_tag)
        msg_unzipped = zloads(body)
        print msg_unzipped["data"]
    #ch.stop_consuming()        
        
   
def main(argv=None):
    desc = u'checker'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-q', '--queue', 
                        help='queue name to get the result', 
                        dest='queue_name', 
                        required=True)
    args = parser.parse_args()
    
    ## remotehost mq.nersc.gov
    creds = pika.PlainCredentials('img', 'synchrotron')
    params = pika.ConnectionParameters(credentials=creds,
                                       host='mq.nersc.gov',
                                       virtual_host='jgi')
    
    conn = pika.BlockingConnection(params)
    ch = conn.channel()
 
    ## Check if all searches are done with n
    ## Declare queue for checking "result.method.message_count"
    #result = ch.queue_declare(queue=args.queue_name, durable=False, auto_delete=True, exclusive=False)
    #result = ch.queue_declare(queue=args.queue_name, durable=True)
    result = ch.queue_declare(queue=args.queue_name, passive=True)

    numTasksInTheTaskQueue = result.method.message_count
    #print "Total number of tasks: ", numTasksInTheTaskQueue
    print numTasksInTheTaskQueue
    conn.close()
    
    return 0

if __name__ == "__main__":
    sys.exit(main())
    
## EOF
