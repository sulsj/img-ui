#!/usr/bin/env python

import sys
try:
    import pika
    #print "Python pika version: ", pika.__version__
except Exception, detail:
    print >>sys.stderr, "Exception: Error importing pika. Please check your installation."
    print >>sys.stderr, "Detail: ", detail
    sys.exit(1)
import cPickle
import zlib



def zdumps(obj):
    """
    dumps pickleable object into zlib compressed string
    """
    return zlib.compress(cPickle.dumps(obj, cPickle.HIGHEST_PROTOCOL), 9)


def zloads(zstr):
    """
    loads pickleable object from zlib compressed string
    """
    return cPickle.loads(zlib.decompress(zstr))


if __name__ == "__main__":
    ## remote broker (mq.nersc.gov)
    creds = pika.PlainCredentials('img', 'synchrotron')
    params = pika.ConnectionParameters(credentials=creds,
                                       host='mq.nersc.gov',
                                       virtual_host='jgi')

    conn = pika.BlockingConnection(params)
    ch = conn.channel()
    # ch.queue_declare(queue='img_task_queue_monitoring', durable=False, auto_delete=True, exclusive=True)
    # ch.queue_declare(queue='img_task_queue_monitoring', durable=True)
    msg="OK"
    msgContainer = {}
    msgContainer["msg"] = msg
    msgZipped = zdumps(msgContainer)
    ch.basic_publish(exchange='',
                     routing_key='img_task_queue_monitoring_mq_checking',
                     body=msgZipped)
    #print " [Client] Check 'mq.nersc.gov'"
    conn.close()

## EOF