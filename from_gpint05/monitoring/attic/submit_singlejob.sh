#!/bin/bash -l

export IMG_SCRIPTS_DIR=/project/projectdirs/genomes/sulsj/test/2012.08.15-img/amy-test/git-cloned/img-ui/from_gpint05/monitoring

i="0"
while [ $i -lt $1 ] ; do 
    ret=`/opt/uge/phoebe/uge/bin/lx-amd64/qsub -cwd  -l high.c,h_rt=00:30:00 -N img_monitoring_worker $IMG_SCRIPTS_DIR/run_img_worker.sh`
    echo $ret
    jobid=`echo $ret | cut -d' ' -f 3 | cut -d'.' -f 1`
    echo "qsub -cwd -l h_rt=01:00:00 -N img_worker $IMG_SCRIPTS_DIR/run_img_worker.sh ==> $jobid"
    if [[ $jobid -eq -1 || -z $jobid ]]; then
        echo "Failed to submit one of the worker jobs."
    else
        echo "Your job is submitted ==> jobid = $jobid"
    fi
    i=$(($i+1))
done
