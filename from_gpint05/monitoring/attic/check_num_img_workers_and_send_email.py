#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# 2012 Seung-Jin Sul

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
import subprocess
import sys
import datetime

def send_error_mail(msgtosend):
    """
    send out diagnostic email
    """

    # create message
    msg = MIMEMultipart()
    msg["subject"] = "IMG Message System Status" 
    msg["From"] = "ssul@lbl.gov" 
    msg["To"] = "sulsj0270@gmail.com"
    
    # compose error message
    body_text = ""
    body_text += msgtosend
    body_text += "\n"
    #print body_text 
    
    body_msg = MIMEText(body_text)
    msg.attach(body_msg)
    
    ## sulsj
    #s = smtplib.SMTP(CFG["SMTPSERVER"])
    s = smtplib.SMTP('smtp.gmail.com',587) #port 465 or 587
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login('sulsj0270@gmail.com','Claire123!')
    s.sendmail("ssul@lbl.gov" , "sulsj0270@gmail.com", msg.as_string()[0:5000])
    s.quit()
    print "Email sent."
    
    
def run_something(cmd):
        """
         
        """
        ##
        ## Run the task
        ##
        try:
            p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        except:
            print "Failed to run user command, %s" % (cmd)
            pass

        stdout_value = "".join(p.stdout.readlines())        
        p.wait()
       
        return stdout_value
    
    
if __name__ == "__main__":
    
    cmd1 = "ps ax | grep -v grep | grep img_monitoring_wk.py | wc -l"
    cmd2 = "ps ax | grep -v grep | grep worker.py | wc -l"
    
    numImgWorkers = run_something(cmd2)
    numMonitoringWorkers = run_something(cmd1)
    
    try:
        numImgWorkers = int(numImgWorkers)
        numMonitoringWorkers = int(numMonitoringWorkers)
    except:
        print "Non-integer values are returned from run_somthing."
        sys.exit(1)
    
    msg = ""
    msg += "num img workers = %s\n" % (str(numImgWorkers))
    msg += "num monitoring workers = %s\n" % (str(numMonitoringWorkers))
    
    if numImgWorkers < 2 or numMonitoringWorkers < 2:    
    #if numImgWorkers != 2:    
        send_error_mail(str(msg))
        
    print "--------"
    print str(datetime.datetime.now())
    print msg

## EOF
        
    
