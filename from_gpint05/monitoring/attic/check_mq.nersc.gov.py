#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# 2012 Seung-Jin Sul

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
import subprocess
import sys
import datetime
import time
import select
import os

class ProcessMonitor:
    def __init__(self):
        self.__process = None
        self.__stdin = None
        self.__stdout = None

    def _create(self, cmd):
        self.__process = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)
        self.__stdin = self.__process.stdout
        self.__stdout = self.__process.stdout

    def _wait(self):
        self.__process.wait()

    def _poll(self):
        self.__process.poll()

    def _close(self):
        os.kill(self.__process.pid,9)

    def _listen(self):
        """
        get from stdout
        """
        return "".join(self.__stdout.readlines())

    def _listen2(self):
        """
        My attempt at trying different things.
        """
        inp, out = self.__process.communicate("")
        return out


def send_error_mail(msgtosend):
    """
    send out diagnostic email
    """

    # create message
    msg = MIMEMultipart()
    msg["subject"] = "Monitoring IMG BG Computation"
    msg["From"] = "ssul@lbl.gov"
    msg["To"] = "sulsj0270@gmail.com"

    # compose error message
    body_text = ""
    body_text += "From check_mq.nersc.gov.py\n"
    body_text += msgtosend
    body_text += "\n"
    #print body_text

    body_msg = MIMEText(body_text)
    msg.attach(body_msg)

    ## sulsj
    #s = smtplib.SMTP(CFG["SMTPSERVER"])
    s = smtplib.SMTP('smtp.gmail.com',587) #port 465 or 587
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login('sulsj0270@gmail.com','')
    s.sendmail("ssul@lbl.gov" , "sulsj0270@gmail.com", msg.as_string()[0:5000])
    s.quit()
    print "ERROR: Email sent"


#def run_something(cmd):
#        """
#
#        """
#
#        ##
#        ## Run the task
#        ##
#        try:
#            p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
#        except:
#            print "Failed to run user command, %s" % (cmd)
#            pass
#
#        stdout_value = "".join(p.stdout.readlines())
#        p.wait()
#
#        return stdout_value


if __name__ == "__main__":

    cmd = "/project/projectdirs/genomes/sulsj/test/2012.08.15-img/amy-test/git-cloned/img-ui/from_gpint05/monitoring/check_mq.nersc.gov.sh"
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    #p.wait()
    #p.poll()

    if not "OK" in stdout and not "Received" in stdout:
        # send_error_mail(str(stdout))
        cmd = "/project/projectdirs/genomes/sulsj/test/2012.08.15-img/amy-test/git-cloned/img-ui/from_gpint05/monitoring/sendmail.sh Failed in check_mq.nersc.gov.sh " + str(stdout)
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        p.wait()
    else:
        #print "--------"
        print str(datetime.datetime.now())
        print "OK"

## EOF

    