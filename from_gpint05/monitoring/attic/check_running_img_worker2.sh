#!/bin/bash -l
module load python

send_email() {
    SUBJECT=$1
    MSG=$2
    EMAIL="sulsj0270@gmail.com"
    EMAILMESSAGE="/tmp/imgemailmessage.txt"
    echo $MSG > $EMAILMESSAGE
    /usr/bin/mail -s "$SUBJECT" "$EMAIL" < $EMAILMESSAGE
    ret=$?
    exit $ret
}

TODAY=$(date)
echo "-------------"
echo $TODAY
numMonitoringWorkers=`ps ax | grep -v grep | grep img_monitoring_wk.py | wc -l`
numImgWorkers=`ps ax | grep -v grep | grep worker.py | wc -l`

if [[ $numImgWorkers < 2 || $numMonitoringWorkers < 2 ]] ; then
    echo "Not enough workers, #IMG workers=$numImgWorkers, #monitoring workers=$numMonitoringWorkers"
    send_email "IMG Monitoring" "Not enough workers, #IMG workers=$numImgWorkers, #monitoring workers=$numMonitoringWorkers"
else
    echo "OK, $numImgWorkers, #IMG workers=$numImgWorkers, #monitoring workers=$numMonitoringWorkers"
fi 