#!/bin/bash -l
module load python

send_email() {
    SUBJECT=$1
    MSG=$2
    EMAIL="sulsj0270@gmail.com"
    EMAILMESSAGE="/tmp/imgemailmessage.txt"
    echo $MSG > $EMAILMESSAGE
    /usr/bin/mail -s "$SUBJECT" "$EMAIL" < $EMAILMESSAGE
    ret=$?
    exit $ret
}

doneFile="check_search_working.sh.done"

cd /project/projectdirs/genomes/sulsj/test/2012.08.15-img/amy-test/git-cloned/img-ui/from_gpint05/monitoring

## Remove done file if exists
if [ -f $doneFile ] ; then
    rm -rf $doneFile
fi

cmd="python ./img_monitoring_cl.py --program geneFuncSetProfile --contact 360 --output job2 --funcset Amino_acid_transport --geneset Human_Anterior"
eval $cmd > /dev/null 2>&1
ret=$?
echo "Return value from img_monitoring_cl.py = $ret"

TODAY=$(date)
echo $TODAY
if [[ $ret != "0" ]] ; then
    echo "Failed to run img_monitoring_cl.py. Email sent."
    send_email "Monitoring IMG BG Computation" "Failed to start a background computation, $cmd"
    exit $rc
else
    sleep 600
    if [ -f $doneFile ] ; then
        modsecs=$(date --utc --reference=$doneFile +%s)
        nowsecs=$(date +%s)
        delta=$(($nowsecs-$modsecs))
        echo "File $doneFile was touched $delta secs ago"
        #if [ $delta -lt 60 ]; then
            #echo "OK"
        #fi
        echo "OK"
    else
        echo "Failed to get the result from a background computation, $cmd"
        send_email "Monitoring IMG BG Computation" "Failed to get the result from a background computation, $cmd"
    fi
fi

# EOF