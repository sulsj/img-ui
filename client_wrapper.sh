#!/bin/bash -l
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

##################
module load python
##################

function usage
{
    echo "Usage: client_wrapper.sh --oid <list of comma separated oids> [--oidfile <oid file name>] [--outfile <output file name>]"
}

if [ $# == 0 ]; then
    usage
    exit 
fi

oid=
oidfile=
outfile=

while [ "$1" != "" ]; do
    case $1 in
        -f | --oidfile )        shift
                                oidfile=$1
                                ;;
        -i | --oid )            shift 
                                oid=$1
                                ;;
        -o | --outfile )        shift 
                                outfile=$1
                                ;;                        
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

if [[ "$oid" != ""  &&  "$oidfile" != "" ]]; then
    python ./client.py --oid $oid --oidfile $oidfile
else 
    if [ "$oidfile" != "" ]; then
        python ./client.py --oidfile $oidfile
    else
        python ./client.py --oid $oid
    fi
fi
ret=$?
exit $ret

# EOF
