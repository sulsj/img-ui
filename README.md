IMG-UI Improvement ======

Improve the IMG UI performance by AMQP (RabbitMQ)


HOWTO RUN ---------

TEST RUN

1. Run "worker.py"

2. Open another terminal and run "client.py" like

    ./client.py --oid 3300000031

This will run your "kegg.pl" on the server side and print out two output file
names.

The client can be run with list of oids like
 
    ./client.py --oid 3300000031, 3300000030

or with a file which has a list of oids like
 
    ./client.py --oidfile oids.txt

or with combining both like
 
    ./client.py --oid 3300000031, 3300000030 --oidfile oids.txt


NOTE

A successful execution of "client.py" will print out a tag number like


    $ ./client.py --oid 3300000031,3300000031 --oidfile oids.txt
     Total number of oids: 3 Queue: 1a34146f-09c4-49c4-9dd3-abeab1e4f06f Tag_id:
     9dfdd955-8997-40ae-9de2-37155cdeab28


CHECK THE RESULT

You can call "check.py" with the queue name, tag number, and the number of oids.

    $ ./check.py -q 1a34146f-09c4-49c4-9dd3-abeab1e4f06f -t
    9dfdd955-8997-40ae-9de2-37155cdeab28 -n 3
   

If the search is done the message system will return all absolute path names to
the files to you. If not, the number of processed requests will be returned.


NOTE

- In this test, it is assumed that the second parameter to "kegg.pl" is
  "assembled". It can be modified quickly if you specify the way to give the
  option to "client.py"
- I've decided to return the created file names to you instead of the content of
  files.


CRONTAB SCRITPS

- The script, "check_running_img_worker.sh", can be used for dynamically
  control the number of workers. Please refer to crontab-scripts/README.md.
