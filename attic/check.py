#!/usr/bin/env python
import uuid
import getopt
import gzip
import cPickle
import zlib
import sys


try:
    import pika
except Exception, detail:
    print "Error importing pika. "
    print "Please check your installation."
    print detail
    sys.exit(1)
    
    
def zdumps(obj):
    """
    dumps pickleable object into zlib compressed string
    """
    return zlib.compress(cPickle.dumps(obj,cPickle.HIGHEST_PROTOCOL),9)
    
def zloads(zstr):
    """
    loads pickleable object from zlib compressed string
    """
    return cPickle.loads(zlib.decompress(zstr))

class RpcClient(object):
    def __init__(self):
        credentials = pika.PlainCredentials('sulsj', 'changeme')
        parameters = pika.ConnectionParameters(credentials=credentials,
                                                                      host='128.55.57.16',
                                                                      virtual_host='img_ui')
        self.connection = pika.BlockingConnection(parameters)
        
        self.channel = self.connection.channel()
        
        result = self.channel.queue_declare(exclusive=True) # exclusive = once disconnected, the queue should be deleted
        #result = self.channel.queue_declare(exclusive=True, durable=True) # exclusive = once disconnected, the queue should be deleted
        self.callback_queue = result.method.queue
        self.channel.basic_consume(self.on_response,
                                                 no_ack=True,
                                                 queue=self.callback_queue)

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body

    def call(self, n):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        print "corr_id = ", self.corr_id

        self.channel.basic_publish( exchange='', # nameless exchange
                                                routing_key='rpc_queue',
                                                properties=pika.BasicProperties(
                                                    reply_to = self.callback_queue,
                                                    correlation_id = self.corr_id,
                                                    #delivery_mode = 2,
                                                ),
                                                body=n)
        #while self.response is None:
            #self.connection.process_data_events()
            
        #return int(self.response)
        if self.response is None:
            self.response = zdumps("None")
            
        return self.response
    
    def call2(self):
        
        if self.response is None:
            self.response = zdumps("None")
            
        return self.response
        
def usage():
    print "Usage: python rpc_kegg_client.py --oid <list of comma separated oids> [--oidfile <oid file name>] [-outfile <output file name>]"
    print "           ex) python multi_otions_test.py --oid 1,2,3,4 --oidfile oids.txt"

def main(argv):
    """
    Parse the command line inputs and call send_info 
        
    @param argv: list of arguments
    @type argv: list of strings
    """
    
    oids = []
    outFileName = ""
    
    try:
        opts, args = getopt.getopt(argv[1:],"hi:o:k:c:",["help","oidfile=","output=","oid=","check="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    
    corr_id_stored = ""
    
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-c", "--check"):
            corr_id_stored = arg
        elif opt in ("-k", "--oid"):
            oids = arg.split(',')
        elif opt in ("-i", "--oidfile"):
            try:
                #print "Loading oids form file,", arg 
                fh = open(arg, "r")
                for line in fh: 
                    if len(line.strip()) > 0:
                        oids.append(line.strip())
                fh.close()
                #print "Total number of oids loaded: ", len(oids)
            except IOError:
                print >> sys.stderr, "ERROR: Cannot open oid file"
        elif opt in ("-o", "--output"):
            outputFileName = arg
        else:
            usage()
            sys.exit(2)
    
    if corr_id_stored:
        b_rpc = RpcClient()
        b_rpc.corr_id = corr_id_stored
        res = b_rpc.call2()
        msg_unzipped = zloads(res)
        print msg_unzipped
        
    else:
        for oid in oids:
            a_rpc = RpcClient()
            #print " [->] Requesting %s..." % (oid,)
            msgContainer = {}
            msgContainer["oid"] = oid
            msgContainer["flag"] = "assembled"
            #msgContainer["oid"] = 3300000030
            #msgContainer["flag"] = "unassembled"
            #msgContainer["chann_name"] = "channel_1"
            msgContainer["outfile"] = outFileName
            msgContainer["data"] = ""
            msg_zipped = zdumps(msgContainer)
            
            #########################
            response = a_rpc.call(msg_zipped)
            #########################
            
            msg_unzipped = zloads(response)
            print " [<-] Got %r for oid %s" % (msg_unzipped,oid,)
            #print msg_unzipped["data"],
    
    print "\n"

if __name__ == "__main__":
    main(sys.argv)   

# EOF



