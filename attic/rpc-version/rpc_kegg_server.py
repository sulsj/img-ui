#!/usr/bin/env python
import sys
import gzip
import cPickle
import zlib
import subprocess
import time

try:
    import pika
except Exception, detail:
    print "Error importing pika. "
    print "Please check your installation."
    print detail
    sys.exit(1)
    
def zdumps(obj):
    """
    dumps pickleable object into zlib compressed string
    """
    return zlib.compress(cPickle.dumps(obj,cPickle.HIGHEST_PROTOCOL),9)
    
def zloads(zstr):
    """
    loads pickleable object from zlib compressed string
    """
    return cPickle.loads(zlib.decompress(zstr))

def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
       return fib(n-1) + fib(n-2)

def echo(msg):
        msg_unzipped = zloads(msg)
        print "Recv'd: ", msg_unzipped
        time.sleep(5)
        msg_zipped = zdumps(msg_unzipped)
        print "Message consumed, Result was sent back."

        return msg_zipped

def run_something(msg_zipped):
        """
        Run kegg.pl with oid
        
        @param msg_zipped: compressed msg from client
        @type msg_zipped: dict with the keys, oid, flag, outfile, data
        """
        ##
        ## uncompress msg to get a task
        msg_unzipped = zloads(msg_zipped)
        oid = msg_unzipped["oid"]
        flag = msg_unzipped["flag"]
        outfile = msg_unzipped["outfile"]
        data = msg_unzipped["data"]
        
        ##
        ## Run the task
        print "Running search..."
        cmd = "kegg.pl " + str(oid) + " " + str(flag)
        #cmd = "/usr/bin/env"
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        stdout_value = ""
        for line in p.stdout.readlines():
            print line
            stdout_value += line
        p.wait()
        print "Search completed."
        
        ##
        ## Prepare result to send back
        msgContainer = {}
        msgContainer["oid"] = oid
        msgContainer["flag"] = flag
        msgContainer["data"] = stdout_value
        msg_zipped2 = zdumps(msgContainer)
        
        return msg_zipped2

def on_request(ch, method, props, body):
        print "[->] Request arrives with oid= %s" % (zloads(body))
        
        #msg_unzipped = zloads(body)
        #print " [.] fib(%s)"  % (n,)
        #response = fib(int(msg_unzipped["oid"]))
        
        response = echo(body)
        
        #response = run_something(body)
            
        ch.basic_publish(exchange='', # nameless exchange
                                 routing_key=props.reply_to,
                                 properties=pika.BasicProperties(correlation_id = props.correlation_id),
                                 #properties=pika.BasicProperties(correlation_id = props.correlation_id,
                                 #                                               delivery_mode = 2,
                                 #                                ),
                                 body=response)
        ch.basic_ack(delivery_tag = method.delivery_tag)


def main(argv):
        """
        Parse the command line inputs and call send_info 
        
        @param argv: list of arguments
        @type argv: list of strings
        """
        
        ## localhost
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        ## remotehost
        #credentials = pika.PlainCredentials('sulsj', 'changeme')
        #parameters = pika.ConnectionParameters(credentials=credentials,
        #                                       host='128.55.57.16',
        #                                       virtual_host='img_ui')
        #connection = pika.BlockingConnection(parameters)

        channel = connection.channel()
        channel.queue_declare(queue='rpc_queue')
        #channel.queue_declare(queue='rpc_queue', durable=True)
        channel.basic_qos(prefetch_count=1) # how many messages send to server
        channel.basic_consume(on_request, queue='rpc_queue')
        
        print "[x] Awaiting RPC requests"
        channel.start_consuming()


if __name__ == "__main__":
    main(sys.argv)   

# EOF

