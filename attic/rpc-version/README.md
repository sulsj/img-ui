img-ui
======

Improve the IMG UI performance by AMQP (RabbitMQ) and multiprocessing


HOWTO RUN
---------

TEST RUN

1. Run "rpc_kegg_server.py"

2. Open another terminal and run "rpc_kegg_client.py" like
    ./rpc_kegg_client.py --oid 3300000031

This will run your "kegg.pl" on the server side and print out two output file names.

The client can be run with list of oids like
    ./rpc_kegg_client.py --oid 3300000031, 3300000030

or with a file which has a list of oids like
    ./rpc_kegg_client.py --oidfile oids.txt

or with combining both like
    ./rpc_kegg_client.py --oid 3300000031, 3300000030 --oidfile oids.txt

NOTE

- In this test, it is assumed that the second parameter to "kegg.pl" is "assembled". It can be modified quickly if you specify the way to give the option to "rpc_kegg_client.py"
- I've decided to return the created file names to you instead of the content of files.

