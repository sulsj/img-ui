#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""

This is to check whether if the requested search work is done and
the result is in the returning queue or not. The tag id is used to verify the recipient.

If ready, URLs for two output files will be sent back to the client
If not, no return.

    Usage: check.py -t <tag id> -q <queue name> -n <num oiods>

"""

import uuid
import gzip
import cPickle
import zlib
import sys
import os
import argparse

try:
    import pika
except Exception, detail:
    print "Error importing pika. "
    print "Please check your installation."
    print detail
    sys.exit(1)
    

g_queue_name = ""
g_tag_id = ""
g_num_requests = ""


def zdumps(obj):
    """
    dumps pickleable object into zlib compressed string
    """
    return zlib.compress(cPickle.dumps(obj,cPickle.HIGHEST_PROTOCOL),9)

    
def zloads(zstr):
    """
    loads pickleable object from zlib compressed string
    """
    return cPickle.loads(zlib.decompress(zstr))


def delete_queue(qName):
    host = '128.55.57.16'
    creds = pika.PlainCredentials('sulsj', 'changeme')
    #params = pika.ConnectionParameters(host, credentials=creds, virtual_host='img_ui', heartbeat=True)
    params = pika.ConnectionParameters(host,
                                       credentials=creds,
                                       virtual_host='taskfarmer-mq',
                                       heartbeat=True)
    #conn = pika.AsyncoreConnection(params)
    conn = pika.BlockingConnection(params)
    ch = conn.channel()
    try:
        ch.queue_delete(queue=qName)
        print "Delete queue, %s" % (qName)
    except:
        print "Failed to queue_delete, %s" % (qName)
    
    try:
        ch.close()
    except:
        pass
        #print "Failed to ch.close()"
    
    try:    
        conn.close()
    except:
        pass
        #print "Failed to conn.close()"
        

def on_request(ch, method, props, body):
    if  (g_tag_id == props.correlation_id):
        ## send ack
        ch.basic_ack(delivery_tag = method.delivery_tag)
        
        #print " [x] Received %r" % (zloads(body),)
        msg_unzipped = zloads(body)
        print msg_unzipped["data"]
        
    #ch.stop_consuming()        
        
        
def main(argv=None):
    ## localhost
    #conn = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    ## remotehost
    credentials = pika.PlainCredentials('img', 'img')
    parameters = pika.ConnectionParameters(credentials=credentials,
                                                                  host='128.55.57.16',
                                                                  virtual_host='img_ui')
    conn = pika.BlockingConnection(parameters)
    ch = conn.channel()
    
    desc = u'checker'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-q', '--queue', help='queue name to get the result', dest='queue_name', required=True)
    parser.add_argument('-t', '--tag', help='tag id ', dest='tag_id', required=True)
    parser.add_argument('-n', '--num', help='total number of requests ', dest='num_req', required=True)
    args = parser.parse_args()
    
    global g_queue_name
    g_queue_name = args.queue_name
    global g_tag_id
    g_tag_id = args.tag_id
    global g_num_requests
    g_num_requests = args.num_req
    
    ## Check if all searches are done with n
    ## Declare queue for checking "result.method.message_count"
    result = ch.queue_declare(queue=g_queue_name, durable=True)
    print "Processed: ", result.method.message_count
    
    ## if all the searches are done, start consuming
    if int(g_num_requests) == result.method.message_count:
        try:
            ch.basic_consume(on_request, queue=g_queue_name)
        except Exception, e:
            print "Basic consume exception: ", e
            sys.exit(1)
    
        ## delete the queue after getting all the results
        delete_queue(g_queue_name)
        
    conn.close()

if __name__ == "__main__":
    sys.exit(main())
    
# EOF