#!/usr/bin/env python
import pika


##
## channel_max: Maximum number of channels to allow, defaults to 0 for None
## frame_max: The maximum byte size for an AMQP frame. Defaults to 131072
## heartbeat: Turn heartbeat checking on or off. Defaults to False.
##

#connection = pika.BlockingConnection(pika.ConnectionParameters(host='128.55.57.16'))
credentials = pika.PlainCredentials('sulsj', 'changeme')
parameters = pika.ConnectionParameters(credentials=credentials,
                                       host='128.55.57.16',
                                       virtual_host='img_ui')
connection = pika.BlockingConnection(parameters)

channel = connection.channel()

channel.queue_declare(queue='rpc_queue')

def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)

def on_request(ch, method, props, body):
    n = int(body)

    print " [.] fib(%s)"  % (n,)
    response = fib(n)

    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id = props.correlation_id),
                     body=str(response))
    ch.basic_ack(delivery_tag = method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(on_request, queue='rpc_queue')

print " [x] Awaiting RPC requests"
channel.start_consuming()

