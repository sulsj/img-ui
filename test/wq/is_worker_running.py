import re
import subprocess

def is_running(process):

    s = subprocess.Popen(["ps", "axw"],stdout=subprocess.PIPE)
    for x in s.stdout:
        if re.search(process, x):
            return True

    return False


if __name__ == "__main__":

    process_name = "worker.py"

    print is_running(process_name)

