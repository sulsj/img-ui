#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""

This is to run a search against the precomputed database using a list of oids.
Two output files will be saved in a location and the URL will be returned to
the queue. The user can come and check the result is ready to the queue using check.py.

"""

import sys
import gzip
import cPickle
import zlib
import subprocess
import time

try:
    import pika
except Exception, detail:
    print "Error importing pika. "
    print "Please check your installation."
    print detail
    sys.exit(1)
    
    
def zdumps(obj):
    """
    dumps pickleable object into zlib compressed string
    """
    return zlib.compress(cPickle.dumps(obj,cPickle.HIGHEST_PROTOCOL),9)

    
def zloads(zstr):
    """
    loads pickleable object from zlib compressed string
    """
    return cPickle.loads(zlib.decompress(zstr))


def run_something(msg_zipped):
        """
        Run kegg.pl with oid
        
        @param msg_zipped: compressed msg from client
        @type msg_zipped: dict with the keys, oid, flag, outfile, data
        """
        ##
        ## uncompress msg to get a task
        msg_unzipped = zloads(msg_zipped)
        oid = msg_unzipped["oid"]
        flag = msg_unzipped["flag"]
        outfile = msg_unzipped["outfile"]
        data = msg_unzipped["data"]
        
        ##
        ## Run the task
        print "Running search..."
        cmd = ' '.join(["kegg.pl", str(oid), str(flag)])
        #cmd = "/usr/bin/env"
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        stdout_value = ""
        for line in p.stdout.readlines():
            print line
            stdout_value += line
        p.wait()
        print "Search completed."
        
        ##
        ## Prepare result to send back
        msgContainer = {}
        msgContainer["oid"] = oid
        msgContainer["flag"] = flag
        msgContainer["data"] = stdout_value
        msg_zipped2 = zdumps(msgContainer)
        
        return msg_zipped2
    

def on_request(ch, method, props, body):
    # send ack
    ch.basic_ack(delivery_tag = method.delivery_tag)
    
    #time.sleep( body.count('.') )
    #time.sleep(5)
    msg_unzipped = zloads(body)
    print " [x] Received %r" % (msg_unzipped,)
    oid = msg_unzipped["oid"]
    flag = msg_unzipped["flag"]
    outfile = msg_unzipped["outfile"]
    data = msg_unzipped["data"]
            
    response = run_something(body)
    #response = "Done"
    ch.basic_publish(exchange='', # nameless exchange
                             routing_key=props.reply_to, # use the queue which the client created
                             properties=pika.BasicProperties(
                                    delivery_mode = 2, # make message persistent
                                    correlation_id = props.correlation_id
                             ),
                             body=response)
    
    print " [X] Send result back."


#def callback(ch, method, properties, body):
#    print " [x] Received %r" % (body,)
#    time.sleep( body.count('.') )
#    print " [x] Done"
#    ch.basic_ack(delivery_tag = method.delivery_tag)


def main(argv):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    
    channel.queue_declare(queue='task_queue', durable=True)
    print ' [*] Waiting for messages. To exit press CTRL+C'
    
    channel.basic_qos(prefetch_count=1)
    #channel.basic_consume(callback, queue='task_queue')
    channel.basic_consume(on_request, queue='task_queue')
    
    channel.start_consuming()


if __name__ == "__main__":
    sys.exit(main(sys.argv))
    
# EOF