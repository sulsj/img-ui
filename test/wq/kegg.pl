#!/usr/bin/env perl
######################################################################
# generate MetaCyc stats for MER-FS genomes
#
######################################################################

use POSIX qw(ceil floor);
use strict;
use Archive::Zip;
use FileHandle;
use BerkeleyDB;

use Data::Dumper; 
use DBI; 
use MIME::Base64 qw( decode_base64 ); 
 
use Cwd;
 
my $taxon_oid = 0;
my $data_type = "assembled";
my $mer_data_dir = "/house/groupdirs/img/normal/img/web/data/mer.fs";
my $zip_cnt = 1000;

if ( scalar(@ARGV) > 0 ) { 
    $taxon_oid = $ARGV[0]; 
} 
else { 
    print "usage: kegg.pl <taxon_oid> <assembled/unassembled>\n"; 
    exit(1); 
} 

if ( scalar(@ARGV) > 1 ) {
    $data_type = $ARGV[1]; 
}

my $data_dir = $mer_data_dir . "/" . $taxon_oid . "/" . $data_type;
my $out_dir = "./";

my $line = "";
my $line_no = 0;

my $out_file_name = "kegg_cate.$taxon_oid.$data_type.txt";
open (OUTPUT_FILE, '>', $out_file_name) or
    die "Cannot open $out_file_name\n";

my $pway_file_name = "kegg_count.$taxon_oid.$data_type.txt";
open (PWAY_FILE, '>', $pway_file_name) or
    die "Cannot open $pway_file_name\n";

my $dsn = "host=gpodb01.nersc.gov;port=1521;sid=imgmer01";
my $user = "img_mer_v340"; 
my $pw = decode_base64("aW1nX21lcl92MzQwOTg3");

my $dbh = DBI->connect( "dbi:Oracle:$dsn", $user, $pw );
if( !defined( $dbh ) ) { 
    die "Cannot log into Oracle database.";
} 
 
$dbh->{ LongTruncOk } = 1;

my %pathway_ko;
my %cate_ko;
my $sql = qq{
        select distinct nvl(kp.category, 'Unknown'), rk.ko_terms
        from kegg_pathway kp, image_roi ir, image_roi_ko_terms rk
        where ir.pathway = kp.pathway_oid
        and ir.roi_id = rk.roi_id
        order by 1, 2
    }; 

my $cur = $dbh->prepare( $sql ) || die "Cannot prepare $sql\n";
$cur->execute( ) || die "Cannot execute $sql\n";
for (my $i = 0; $i < 10000000000; $i++) {
    my ($pathway_cate, $ko_id) = $cur->fetchrow( );
    last if !$pathway_cate;

    if ( $cate_ko{$pathway_cate} ) {
	my $s = $cate_ko{$pathway_cate} . "\t" . $ko_id;
	$cate_ko{$pathway_cate} = $s;
    }
    else {
	$cate_ko{$pathway_cate} = $ko_id;
    }
}
$cur->finish();
my @cate_ids = (keys %cate_ko);


$sql = qq{
        select distinct kp.pathway_oid, rk.ko_terms
        from kegg_pathway kp, image_roi ir, image_roi_ko_terms rk
        where ir.pathway = kp.pathway_oid
        and ir.roi_id = rk.roi_id
        order by 1, 2
    }; 

$cur = $dbh->prepare( $sql ) || die "Cannot prepare $sql\n";
$cur->execute( ) || die "Cannot execute $sql\n";
for (my $i = 0; $i < 10000000000; $i++) {
    my ($pathway_oid, $ko_id) = $cur->fetchrow( );
    last if !$pathway_oid;

    if ( $pathway_ko{$pathway_oid} ) {
	my $s = $pathway_ko{$pathway_oid} . "\t" . $ko_id;
	$pathway_ko{$pathway_oid} = $s;
    }
    else {
	$pathway_ko{$pathway_oid} = $ko_id;
    }
}
$cur->finish();
my @pathway_oids = (keys %pathway_ko);

$dbh->disconnect();

$line = "";
$line_no = 0;

### ko
my %gene_ko_h;

my %ko_h;
my $db_file_name = $mer_data_dir . "/" . $taxon_oid . "/$data_type/ko_genes.db";
if ( -e $db_file_name ) { 
    tie %ko_h, "BerkeleyDB::Hash",
    -Filename => $db_file_name,
    -Flags    => DB_RDONLY,
    -Property  => DB_DUP
	or die "Cannot tie $db_file_name\n";
} 
else { 
    die "No ko_genes.db for this taxon\n";
} 

my @kos = (keys %ko_h);
for my $k2 (sort @kos) {
    #print "Processing KO $k2 ...\n";
    my @gene_list = split(/\t/, $ko_h{$k2});
    for my $g2 ( @gene_list ) {
		if ( $gene_ko_h{$g2} ) {
			$gene_ko_h{$g2} .= "," . $k2;
		}
		else {
			$gene_ko_h{$g2} = $k2;
		}
    }
}

untie %ko_h;

my $total = 0;
my $cnt0 = 0;
my %pathway_gene_cnt;
for my $gene_oid (keys %gene_ko_h) {
    $cnt0++;
    #if ( ($cnt0 % 1000) == 0 ) {
	#	print "$cnt0 genes processed ...\n";
    #}

    my @gene_ko = split(/\,/, $gene_ko_h{$gene_oid});
    my %h2;
    for my $ko (@gene_ko) {
	$h2{$ko} = 1;
    }

    my $gene_has_kegg = 0;
    for my $cate_id ( @cate_ids ) {
	my @pway_kos = split(/\t/, $cate_ko{$cate_id});
	for my $ko2 ( @pway_kos ) {
	    if ( $h2{$ko2} ) {
		# gene_oid has ko
		if ( $pathway_gene_cnt{$cate_id} ) {
		    $pathway_gene_cnt{$cate_id} += 1;
		}
		else {
		    $pathway_gene_cnt{$cate_id} = 1;
		}
		$gene_has_kegg = 1;
		last;
	    }
	}
    }

    if ( $gene_has_kegg ) {
	$total++;
    }
}

for my $cate_id ( sort @cate_ids ) {
    if ( $pathway_gene_cnt{$cate_id} ) {
	print OUTPUT_FILE $cate_id . "\t" . $pathway_gene_cnt{$cate_id} . "\n";
    }
}
print OUTPUT_FILE "total\t$total\n";
close OUTPUT_FILE;
print getcwd . "/" . $out_file_name . "\n";

###########################################################################

$total = 0;
$cnt0 = 0;
undef %pathway_gene_cnt;
for my $gene_oid (keys %gene_ko_h) {
    $cnt0++;
    #if ( ($cnt0 % 1000) == 0 ) {
	#	print "$cnt0 genes processed ...\n";
    #}

    my @gene_ko = split(/\,/, $gene_ko_h{$gene_oid});
    my %h2;
    for my $ko (@gene_ko) {
	$h2{$ko} = 1;
    }

    my $gene_has_kegg = 0;
    for my $pathway_oid ( @pathway_oids ) {
	my @pway_kos = split(/\t/, $pathway_ko{$pathway_oid});
	for my $ko2 ( @pway_kos ) {
	    if ( $h2{$ko2} ) {
		# gene_oid has ko
		if ( $pathway_gene_cnt{$pathway_oid} ) {
		    $pathway_gene_cnt{$pathway_oid} += 1;
		}
		else {
		    $pathway_gene_cnt{$pathway_oid} = 1;
		}
		$gene_has_kegg = 1;
		last;
	    }
	}
    }

    if ( $gene_has_kegg ) {
	$total++;
    }
}

for my $pathway_oid (sort {$a <=> $b} (@pathway_oids)) {
    if ( $pathway_gene_cnt{$pathway_oid} ) {
	print PWAY_FILE $pathway_oid . "\t" . $pathway_gene_cnt{$pathway_oid} . "\n";
    }
}
print PWAY_FILE "total\t$total\n";
close PWAY_FILE;
print getcwd . "/" . $pway_file_name . "\n";


###########################################################################
# blankStr - Is blank string.  Return 1=true or 0=false.
###########################################################################   
sub blankStr { 
    my $s = shift;
 
    if ($s =~ /^[ \t\n]+$/ || $s eq "") {
        return 1; 
    } 
    else {
        return 0;
    } 
} 
 
#############################################################################
# isInt - Is integer.                                                        
############################################################################# 
sub isInt { 
    my $s = shift;
 
    if( $s =~ /^\-{0,1}[0-9]+$/) { 
        return 1;
    } 
    elsif( $s =~ /^\+{0,1}[0-9]+$/) {
        return 1;
    } 
    else { 
        return 0;
    }
}


#############################################################################
# strTrim
#############################################################################
sub strTrim { 
    my $s = shift; 
 
    $s =~ s/^\s+//; 
    $s =~ s/\s+$//; 
    return $s; 
} 


#############################################################################
# getUnzipFileName
#############################################################################
sub getUnzipFileName {
    my $s = shift;

    $s = strTrim($s);
    my ($size, $date1, $time1, $name1) = split(/[ \t\s]+/, $s, 4);

    return $name1;
}


1;

