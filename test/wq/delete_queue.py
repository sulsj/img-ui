import pika
import sys

#host = 'localhost'
#creds = pika.PlainCredentials('sulsj', 'changeme')
#params = pika.ConnectionParameters(host, credentials=creds, heartbeat=False)
#conn = pika.AsyncoreConnection(params)
params = pika.ConnectionParameters(host='localhost')
conn = pika.BlockingConnection(params)
ch = conn.channel()

try:
    ch.queue_delete(queue=sys.argv[1], nowait=True)
except:
    conn.close()
    
ch.close()
conn.close()
