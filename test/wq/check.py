#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""

This is to check whether if the requested search work is done and
the result is in the returning queue or not. The tag id is used to verify the recipient.

If ready, URLs for two output files will be sent back to the client
If not, no return.

    Usage: client.py <tag id>

"""

import uuid
import getopt
import gzip
import cPickle
import zlib
import sys


try:
    import pika
except Exception, detail:
    print "Error importing pika. "
    print "Please check your installation."
    print detail
    sys.exit(1)
    
    
def zdumps(obj):
    """
    dumps pickleable object into zlib compressed string
    """
    return zlib.compress(cPickle.dumps(obj,cPickle.HIGHEST_PROTOCOL),9)

    
def zloads(zstr):
    """
    loads pickleable object from zlib compressed string
    """
    return cPickle.loads(zlib.decompress(zstr))


def on_request(ch, method, props, body):
    #print props.correlation_id
    
    if  (sys.argv[1] == props.correlation_id):
        # send ack
        ch.basic_ack(delivery_tag = method.delivery_tag)
        
        print " [x] Received %r" % (zloads(body),)
      
        #if body == "Done":
            #ch.queue_delete(queue=queue_name, if_unused=False, if_empty=False, nowait=True)
    ch.stop_consuming()

conn = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
ch = conn.channel()

#queue_name = sys.argv[1]
queue_name = "res_queue"
corr_id = sys.argv[1]

## There must be the queue "queue_name"
## Thus don't need to queue_declare
#channel.queue_declare(queue=queue_name, durable=True)
#print " [*] Checking for messages."

ch.basic_qos(prefetch_count=1)
try:
    ch.basic_consume(on_request, queue=queue_name)
except Exception, e:
    #print "Basic consume exception: ", e
    sys.exit(1)

## To do
#print "Delete queue, ", queue_name
##queue_delete(callback=None, ticket=0, queue='', if_unused=False, if_empty=False, nowait=False)
#ch.queue_delete(queue=queue_name, if_unused=False, if_empty=False, nowait=True)
#ch.close()
conn.close()

