#!/bin/bash


function usage
{
    echo "Usage"
}

oid=
oidfile=

while [ "$1" != "" ]; do
    case $1 in
        -f | --oidfile )        shift
                                oidfile=$1
                                ;;
        -i | --oid )            shift 
                                oid=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

echo $oid
echo $oidfile
exit

if [ $# -gt 0 ]; then
    echo $#
else
    echo "usage"
fi

if [ "$1" != "" ]; then
    echo $1
fi

if [ "$2" != "" ]; then
    echo $2
fi

echo $1 $2

set ret_code = 1
exit $ret_code

