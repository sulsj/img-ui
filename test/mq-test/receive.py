#!/usr/bin/env python
import pika

## localhost
#connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
## remotehost
#credentials = pika.PlainCredentials('img', 'img')
#parameters = pika.ConnectionParameters(credentials=credentials,
                                       #host='128.55.57.16',
                                       #virtual_host='img_ui')
## remote broker (carver)
#credentials = pika.PlainCredentials('taskfarmer-mq', 'taskfarmer-mq')
#parameters = pika.ConnectionParameters(credentials=credentials,
#                                       host='128.55.57.16',
#                                       virtual_host='taskfarmer-mq')

## remote broker (mq.nersc.gov)
credentials = pika.PlainCredentials('sulsj', 'synchrotron')
parameters = pika.ConnectionParameters(credentials=credentials,
                                       host='mq.nersc.gov',
                                       virtual_host='jgi')

connection = pika.BlockingConnection(parameters)

channel = connection.channel()

channel.queue_declare(queue='test', durable=False, auto_delete=True, exclusive = False)

print ' [*] Waiting for messages. To exit press CTRL+C'


def callback(ch, method, properties, body):
    print " [x] Received %r" % (body,)

channel.basic_consume(callback, queue='test', no_ack=True)

channel.start_consuming()
