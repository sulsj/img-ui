import logging
import time


logger = logging.getLogger("mylog")
formatter = logging.Formatter('%(asctime)s | %(name)s |  %(levelname)s: %(message)s')
logger.setLevel(logging.DEBUG)

streamLogger = logging.StreamHandler()
streamLogger.setLevel(logging.WARNING)
streamLogger.setFormatter(formatter)

logFilePath = "my.log"
#fileLogger = logging.handlers.TimedRotatingFileHandler(filename = logFilePath, when = 'midnight', backupCount = 30)
fileLogger = logging.FileHandler(logFilePath)
fileLogger.setFormatter(formatter)
fileLogger.setLevel(logging.DEBUG)

logger.addHandler(fileLogger)
logger.addHandler(streamLogger)

logger.info("Started");
try:
    x = 14
    y = 0
    z = x / y
except Exception as ex:
    logger.error("Operation failed.")
    logger.debug("Encounetered {0} when trying to perform calculation.".format(ex))

logger.info("Ended");

